import 'package:dio/dio.dart';

abstract class Failure {
  final String massege;

  Failure(this.massege);
}

class ServerError extends Failure {
  ServerError(super.massege);

  factory ServerError.fromDioException(DioException dioError) {
    switch (dioError.type) {
      case DioExceptionType.receiveTimeout:
        return ServerError("Receive Timeout with Api Server");
      case DioExceptionType.connectionTimeout:
        return ServerError("Connection Timeout with Api Server");
      case DioExceptionType.sendTimeout:
        return ServerError("Send Timeout with Api Server");
      case DioExceptionType.badResponse:
        return ServerError.fromresponseError(
            dioError.response!.statusCode!, dioError.response!.data);
      case DioExceptionType.cancel:
        return ServerError("Receive  timeout to api server is cancelled");
      case DioExceptionType.unknown:
        // if (dioError.message!.contains("SocketException")) {
        //   return ServerError("No Internet Connection");
        // }

        return ServerError("Unexpected error please try again");

      default:
        return ServerError("Oops there was an error pleas try again");
    }
  }

  factory ServerError.fromresponseError(int? statusCode, dynamic response) {
    if (statusCode == 400 || statusCode == 401 || statusCode == 403) {
      return ServerError(response["error"]["message"]);
    } else if (statusCode == 404) {
      return ServerError("Your Request not Found,Please try Again");
    } else if (statusCode == 500) {
      return ServerError("Internal Server error,Please try later");
    } else {
      return ServerError("Receive  timeout to api server is cancelled");
    }
  }
}
