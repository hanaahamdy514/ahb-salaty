import 'package:flutter/cupertino.dart';
import 'package:task1/core/utils/app_colors.dart';

abstract class Styles {
  static const textStyle18 =
      TextStyle(color: AppColors.lightBlack, fontSize: 18);
  static const textStyle13 =
      TextStyle(color: AppColors.lightBlack, fontSize: 13);
  static const textStyle22 =
      TextStyle(color: AppColors.primaryColor, fontSize: 22);
  static const textStyle20 = TextStyle(
      color: AppColors.primaryColor, fontSize: 20, fontWeight: FontWeight.w600);
  static const textStyle16 =
      TextStyle(color: AppColors.lightBlack, fontSize: 16);
  static const langTextStyle16 = TextStyle(
      color: AppColors.green, fontSize: 16, fontWeight: FontWeight.w500);
  static const textStyle14 = TextStyle(
      color: AppColors.lightBlack, fontSize: 14, fontWeight: FontWeight.w500);
  static const textStyle12= TextStyle(
      color: AppColors.lightBlack, fontSize: 12, );
}

