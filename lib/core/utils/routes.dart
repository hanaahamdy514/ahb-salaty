import 'package:go_router/go_router.dart';
import 'package:task1/features/auth/presentation/views/active_account.dart';
import 'package:task1/features/auth/presentation/views/register.dart';
import 'package:task1/features/auth/presentation/views/enter_phone_number_reset_password.dart';
import 'package:task1/features/auth/presentation/views/reset_password.dart';
import 'package:task1/features/welcome/presentation/views/splash.dart';
import ' widgets/test.dart';
import '../../features/auth/presentation/views/login.dart';

import '../../features/home/presentation/views/home.dart';
import '../../features/welcome/presentation/views/onBoard.dart';
import '../../features/welcome/presentation/views/select_client.dart';

abstract class AppRoutes {
  static final routes = GoRouter(routes: [

    GoRoute(
      path: '/',
      builder: (context, state) => const Splash(),
    ),
    GoRoute(
      path: '/activeAccount',
      builder: (context, state) =>  ActiveAccount(),
    ),
    GoRoute(
      path: '/onboard',
      builder: (context, state) => const OnBoard(),
    ),
    GoRoute(
      path: '/selectClient',
      builder: (context, state) => const SelectClient(),
    ),
    GoRoute(
      path: '/Login',
      builder: (context, state) => Login(),
    ),
    GoRoute(
      path: '/register',
      builder: (context, state) => const Register(),
    ),
    GoRoute(
        path: '/resetPassword', builder: (context, state) => ResetPassWord()),
    GoRoute(
        path: '/enterPhoneToResetPassword',
        builder: (context, state) => PhoneNumberToResetPassword()),
    GoRoute(path: '/home', builder: (context, state) => const Home()),
  ]);
}
