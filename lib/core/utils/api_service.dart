import 'package:dio/dio.dart';
import 'package:task1/core/utils/api_constants.dart';
import 'package:task1/core/utils/constants.dart';

class ApiService {
  final Dio dio;

  ApiService()
      : dio = Dio(
    BaseOptions(
        baseUrl: ApiConstants.baseUrl,
        headers: {
          "Accept": "*/*",
          "Connection": "keep-alive",
          "Content-Type": "application/json",
        },
        connectTimeout: const Duration(seconds: 12),
        sendTimeout: const Duration(seconds: 12)),
  );

  Future get({required String endPoint,
    Map<String, dynamic>? quary,
    String? token}) async {
    dio.options.headers = {"Authorization": "Bearer $token"};
    var response = await dio.get(endPoint, queryParameters: quary);
    return response.data;
  }

  Future postData({required endPoint,
    required Map<String, dynamic> data,
    String?token,
    Map<String, dynamic>? query}) async {
    dio.options.headers = {"Authorization": "Bearer $token"};
    var response = await dio.post(endPoint, data: data, queryParameters: query);
    return response.data;
  }

  // Future postData(
  //     {required endPoint,
  //     required Map<String, dynamic> data,
  //     Map<String, dynamic>? query}) async {
  //   dio.options.headers = {"Authorization": "Bearer $father_token"};
  //   var response = await dio.post(endPoint, data: data, queryParameters: query);
  //   return response.data;
  // }

  Future deleteData({required endPoint, Map<String, dynamic>? query,String? token}) async {
    dio.options.headers = {"Authorization": "Bearer $token"};
    var response = await dio.delete(
      endPoint,
      queryParameters: query,
    );
    return response.data;
  }

  Future patch({required endPoint, required language,String?token}) async {
    dio.options.headers = {"Authorization": "Bearer $token"};
    print(father_token);
    var response =
    await dio.patch(endPoint, queryParameters: {"lang": language});
    return response.data;
  }
}
