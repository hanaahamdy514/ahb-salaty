class ApiConstants {
  static const baseUrl = "https://ilovemyprayers.aait-sa.com/api/";
  static const register = "father/sign-up";
  static const parentLogin = "father/sign-in";
  static const childLogin = "sign-in";
  static const activationCode = "father/activate";
  static const resendCode = "father/resend-code";
  static const resetPassword = "father/reset-password";
  static const changeLanguage = "father/change-lang";
  static const changePassword = "father/update-passward";
  static const updateProfile = "father/update-profile";
  static const updateSonProfile = "update-profile";

  static const fatherSignOut = "father/sign-out";
  static const sonSignOut = "sign-out";

  static const getFatherProfile = "father/profile";
  static const getSonProfile = "profile";
}
