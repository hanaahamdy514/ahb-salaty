import 'package:flutter/material.dart';
import 'package:task1/core/utils/images_path.dart';

class CustomLogo extends StatelessWidget {
  const CustomLogo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Image.asset(
          "assets/images/logo.png",
          // width: screenWidth * .3,
          height: screenHeight * .15,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
