// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
// import 'package:task1/translation/locale_keys.g.dart';
//
// class Test extends StatelessWidget {
//   const Test({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Column(
//
//         children: [
//           SizedBox(height: 60,),
//           Text(LocaleKeys.name.tr(),),
//           Row(
//             children: [
//
//               ElevatedButton(onPressed: () async{
//                 await context.setLocale(Locale("ar"));
//               }, child: Text("ar")),
//               ElevatedButton(onPressed: ()async {
//                 await context.setLocale(Locale("en"));
//               }, child: Text("en"))
//             ],
//           ),
//         ],
//       ),
//     );
//   }
// }