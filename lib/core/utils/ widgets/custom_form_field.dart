import 'package:flutter/material.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/core/utils/styles.dart';
import 'package:task1/generated/l10n.dart';

class CustomFormField extends StatelessWidget {
  const CustomFormField(
      {Key? key,
      required this.hint,
      this.validator,
      required this.controller,
      this.keyboardType,
      this.prefixIcon,
      this.isVisible,
      this.suffix,
      this.fillColor,
       this.num_lines
      })
      : super(key: key);
  final String hint;
  final int? num_lines;
  final TextInputType? keyboardType;
  final String Function(String?)? validator;
  final TextEditingController controller;
  final Widget? prefixIcon;
  final bool? isVisible;
  final Widget? suffix;
  final Color? fillColor;


  @override
  Widget build(BuildContext context) {
    double scrrenHeight = MediaQuery.of(context).size.height;
    return SizedBox(
      height: scrrenHeight * .1,
      child: TextFormField(

        style: Styles.textStyle16.copyWith(
          fontSize: 15,
        ),
        obscureText: isVisible ?? false,
        keyboardType: keyboardType,
        cursorColor: AppColors.primaryColor,
        validator: (value) {
          if (value?.isEmpty ?? true) {
            return S.of(context).enter_required_data;
          } else {
            return null;
          }
        },
        controller: controller,
        decoration: InputDecoration(
          suffixIcon: suffix,
          contentPadding: const EdgeInsets.all(8),
          prefixIcon: prefixIcon,
          hintText: hint,
          hintStyle: const TextStyle(),
          border: const OutlineInputBorder(borderSide: BorderSide.none),
          enabledBorder: const OutlineInputBorder(borderSide: BorderSide.none),
          focusedBorder: const OutlineInputBorder(borderSide: BorderSide.none),
          fillColor: fillColor ?? AppColors.white,
          filled: true,
        ),
      ),
    );
  }
}
