import 'package:flutter/material.dart';
import 'package:task1/core/utils/images_path.dart';
import '../app_colors.dart';

class CustomHomeBody extends StatelessWidget {
  const CustomHomeBody({Key? key, required this.child}) : super(key: key);
final Widget child;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Container(
            width: double.infinity,
            decoration: const BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(70),
                    topRight: Radius.circular(70))),
            child: child,
          ),
          Positioned(
            left: 130,
            right: 130,
            top: -50,
            child: ClipRRect(
              clipBehavior: Clip.antiAliasWithSaveLayer,
              borderRadius: BorderRadius.circular(25),
              child: Image.asset(
                AppImages.logo,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

