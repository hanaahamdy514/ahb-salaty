import 'package:flutter/material.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/core/utils/styles.dart';

class DefaultButton extends StatelessWidget {
  const DefaultButton({
    Key? key,
    this.onPressed,
    required this.buttonLabel,
    this.buttonBgColor,
    this.buttonLabelColor,
  }) : super(key: key);
  final void Function()? onPressed;
  final String buttonLabel;
  final Color? buttonBgColor;
  final buttonLabelColor;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.height;

    return ElevatedButton(
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5)),
            minimumSize: const Size(double.infinity, 40),
            backgroundColor: buttonBgColor ?? AppColors.primaryColor,
            foregroundColor: buttonLabelColor),
        onPressed: onPressed,
        child: Text(
          buttonLabel,
          style: Styles.textStyle16.copyWith(color: AppColors.white),
        ));
  }
}
