import 'package:flutter/material.dart';
import 'package:task1/core/utils/styles.dart';
import 'package:task1/generated/l10n.dart';

class ForgetPasswordButton extends StatelessWidget {
  const ForgetPasswordButton({Key? key, required this.buttonAction})
      : super(key: key);
  final void Function()? buttonAction;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 30,
      child: TextButton(

        style: TextButton.styleFrom(

            padding: EdgeInsets.zero),
        onPressed: buttonAction,

        child:  Text(
        S.of(context).forgetpassword,
          style: Styles.textStyle13.copyWith(decoration:TextDecoration.underline ),
        ),

      ),
    );
  }
}
