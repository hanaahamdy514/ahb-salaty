import 'package:flutter/material.dart';
import 'package:task1/core/utils/%20widgets/custom_logo.dart';
import 'package:task1/core/utils/images_path.dart';

class IslamicHeader extends StatelessWidget {
  const IslamicHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Image.asset(
          AppImages.islamicHeader,
          fit: BoxFit.cover,
        ),
        const CustomLogo(),
      ],
    );
  }
}
