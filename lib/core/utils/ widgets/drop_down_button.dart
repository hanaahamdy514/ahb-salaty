import 'package:flutter/material.dart';
import 'package:task1/core/utils/styles.dart';

import '../../../features/auth/presentation/view_model/login/login_cubit.dart';

DropdownButtonHideUnderline dropDownItem(var cubit) {
  return DropdownButtonHideUnderline(
    child: DropdownButton(
      icon:const Icon(Icons.expand_more),
      style: Styles.textStyle13,
      padding: const EdgeInsets.only(left: 10),
      autofocus: true,
      items: [20, 966,]
          .map(
            (e) => DropdownMenuItem(
              value: e,
              child: Text("$e"),
            ),
          )
          .toList(),
      value: cubit.selectedCountryCode,
      onChanged: (value) {
        cubit.changeSelectedCountryCode(value);
        debugPrint('$value');
      },
    ),
  );
}
