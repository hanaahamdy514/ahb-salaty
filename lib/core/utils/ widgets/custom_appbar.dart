import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/core/utils/styles.dart';

class CustomAppbar extends StatelessWidget {
  CustomAppbar(
      {Key? key,
      required this.appBarTitle,
      this.leading,
      this.trailing,
      this.leadingAction,
      this.trailingAction,
      this.titleColor})
      : super(key: key);
  String appBarTitle;
  final Color? titleColor;
  final IconData? leading;
  final IconData? trailing;
  final void Function()? leadingAction;
  final void Function()? trailingAction;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        IconButton(
          onPressed: leadingAction,
          icon: Icon(leading),
        ),
        Text(
          appBarTitle,
          style: titleColor == null
              ? Styles.textStyle20.copyWith(color: AppColors.lightBlack)
              : Styles.textStyle20.copyWith(color: titleColor),
        ),
        IconButton(
          icon: trailing == null
              ? const Icon(
                  Icons.navigate_next_sharp,
                  size: 22,
                )
              : Icon(trailing),
          onPressed: trailingAction,
        )
      ],
    );
  }
}
