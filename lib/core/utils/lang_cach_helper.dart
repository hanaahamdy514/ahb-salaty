import 'package:shared_preferences/shared_preferences.dart';
import 'package:task1/core/utils/local_db.dart';

class LanguageCacheHelper {
  //********************cache selected user lang**************//
  Future<void> cacheLanguageCode(String languageCode) async {
    final sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("LOCALE", languageCode);
  }

  //*********************return selected lang *****************//
  Future getCachedLanguageCode() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    final cachedLanguageCode = sharedPreferences.getString("LOCALE");
    if (cachedLanguageCode != null) {
      return cachedLanguageCode;
    } else {
      return "en";
    }
  }
}
