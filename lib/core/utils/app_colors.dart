import 'package:flutter/material.dart';

class AppColors {
  static const Color pink = Color(0xffF1C3CD);
  static const Color darkBlack = Color(0xff000000);
  static const Color lightBlue = Color(0xffC5E8E4);
  static const Color darkWhite = Color(0xffF0F0F0);
  static const Color white = Color(0xffFFFFFF);
  static const Color lightBlack = Color(0xff343434);
  static const primaryColor = Color(0xff70C0C1);
  static const info = Color(0xffDE6C2F);
  static const green = Color( 0xff73AF00);
  static const darkBlue = Color( 0xff41479B);
  static const grey = Color(  0xff8D8D8D);



}
