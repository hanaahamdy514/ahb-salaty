import 'package:get_it/get_it.dart';
import 'package:task1/core/utils/api_service.dart';
import 'package:task1/features/auth/data/repo/reset_password/reset_password_implemntation.dart';
import 'package:task1/features/auth/data/repo/sign_out/sign_out_repo_implementation.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/data/repo/change_password_repo.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/data/repo/change_password_repo_implementation.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/data/repo/change_lang_repo_implementation.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/data/repo/update_profile_repo_impl.dart';
import '../../features/auth/data/repo/active_account/active_acoount_implemntation.dart';
import '../../features/auth/data/repo/login/login_repo_implementtion.dart';
import '../../features/auth/data/repo/register/register_repo_implementation.dart';

final getIt = GetIt.instance;

void setupServiceLocator() {
  getIt.registerSingleton<ApiService>(ApiService());
  getIt.registerSingleton<RegisterRepoImplementation>(
      RegisterRepoImplementation(getIt.get<ApiService>()));
  getIt.registerSingleton<LoginRepoImplementation>(
      LoginRepoImplementation(getIt.get<ApiService>()));
  getIt.registerSingleton<ActiveAccountImplementation>(
      ActiveAccountImplementation(getIt.get<ApiService>()));
  getIt.registerSingleton<SignOutRepoImplementation>(
      SignOutRepoImplementation(getIt.get<ApiService>()));
  getIt.registerSingleton<ResetPasswordRepoImplementation>(
      ResetPasswordRepoImplementation(getIt.get<ApiService>()));
  getIt.registerSingleton<ChangeLanguageRepoImp>(
      ChangeLanguageRepoImp(getIt.get<ApiService>()));
  getIt.registerSingleton<ChangePasswordRepoImp>(
      ChangePasswordRepoImp(getIt.get<ApiService>()));
  getIt.registerSingleton<UpdateProfileRepoImpl>(
     UpdateProfileRepoImpl(getIt.get<ApiService>()));
}
