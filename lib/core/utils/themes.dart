import 'package:flutter/material.dart';
import 'package:task1/core/utils/app_colors.dart';

ThemeData appTheme() {
  return ThemeData(
    scaffoldBackgroundColor: AppColors.darkWhite,
    fontFamily: "URW DIN Arabic",
    colorScheme: ColorScheme.fromSeed(seedColor: AppColors.lightBlack),
    useMaterial3: true,
  );
}
