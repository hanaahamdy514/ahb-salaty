abstract class AppImages {
  static const baseUrl = "assets/images";
  static const islamicBackground = "$baseUrl/islamic_background.png";
  static const go = "$baseUrl/go.png";
  static const islamicHeader = "$baseUrl/islamic_header.png";
  static const starShape = "$baseUrl/star_shape.png";
  static const logo = "$baseUrl/logo.png";
  static const onBoard = "$baseUrl/on_board.png";
  static const eng = "$baseUrl/eng.png";
  static const ar = "$baseUrl/arab.png";
  static const inst = "$baseUrl/inst_logo.png";
  static const twitter = "$baseUrl/twitter.png";

}
