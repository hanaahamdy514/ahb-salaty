import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/features/welcome/presentation/view_model/select_client/select_client_states.dart';

class SelectClientCubit extends Cubit<SelectClientStates> {
  SelectClientCubit() : super(SelectClientInitStateState());
  static SelectClientCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  bool clientCategory = false;

  void selectClientAction(bool selected) {
    clientCategory = selected;
    print("${selected}+##cubit");
    print("${clientCategory}+##cubit");
    emit(SelectClientChangeState());
  }
}
