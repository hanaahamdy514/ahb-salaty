import 'package:flutter/material.dart';
import 'package:task1/features/welcome/presentation/views/widgets/splash_body.dart';

class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: SplashViewBody(),
    );
  }
}
