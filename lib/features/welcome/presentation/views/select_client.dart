import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:task1/core/utils/api_constants.dart';
import 'package:task1/core/utils/api_service.dart';
import 'package:task1/core/utils/service_locator.dart';

import 'package:task1/features/welcome/presentation/views/widgets/select_client_body.dart';

class SelectClient extends StatelessWidget {
  const SelectClient({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:  SelectClientViewBody());
  }
}
