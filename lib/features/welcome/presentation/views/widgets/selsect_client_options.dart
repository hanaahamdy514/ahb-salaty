import 'package:flutter/material.dart';
import 'package:task1/core/utils/images_path.dart';

class SelectClientOptions extends StatefulWidget {
  const SelectClientOptions(
      {Key? key,
      required this.clientLabel,
      required this.clientCategory,
      required this.onPressed})
      : super(key: key);
  final String clientLabel;
  final bool clientCategory;
  final void Function()? onPressed;

  @override
  State<SelectClientOptions> createState() => _SelectClientOptionsState();
}

class _SelectClientOptionsState extends State<SelectClientOptions> {
  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: widget.onPressed,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Image.asset(
              AppImages.starShape,
            ),
            Text(widget.clientLabel)
          ],
        ));
  }
}
