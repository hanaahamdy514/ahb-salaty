import 'package:flutter/material.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/core/utils/images_path.dart';
import 'package:task1/core/utils/styles.dart';
import 'package:task1/features/welcome/presentation/views/widgets/onboard_button.dart';
import 'package:task1/generated/l10n.dart';
class OnBoardingVieWBody extends StatelessWidget {
  const OnBoardingVieWBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: screenWidth * .05),
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            SizedBox(
              height: screenHeight * .03,
            ),
            Image.asset(
              AppImages.onBoard,
              width: screenWidth * .7,
              height: screenHeight * .4,
              fit: BoxFit.cover,
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 27, vertical: 24),
              decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(15)),
              child: Column(
                children: [
                  Text(

                     S.of(context).tell_your_children,
                     style: Styles.textStyle18.copyWith(
                       fontWeight: FontWeight.bold,
                     ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    // LocaleKeys.confirm.tr(),
                     S.of(context).on_board_subTitle,
                    textAlign: TextAlign.center,
                    style:
                        const TextStyle(fontSize: 13, color: Color(0xff575757)),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const OnBoardButton()
          ],
        ),
      ),
    );
  }
}
