import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/images_path.dart';

class OnBoardButton extends StatelessWidget {
  const OnBoardButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return TextButton(
      child: Image.asset(
        AppImages.go,
        height: screenHeight * .08,
      ),
      onPressed: () {
        GoRouter.of(context).push('/selectClient');
      },
    );
  }
}
