import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/images_path.dart';

class SplashViewBody extends StatefulWidget {
  const SplashViewBody({Key? key}) : super(key: key);

  @override
  State<SplashViewBody> createState() => _SplashViewBodyState();
}

class _SplashViewBodyState extends State<SplashViewBody> {
  @override
  void initState() {
    // TODO: implement initState
    Future.delayed(const Duration(seconds: 5), () {
      GoRouter.of(context).push("/onboard");
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Stack(
      alignment: Alignment.center,
      children: [
        SizedBox(
          width: double.infinity,
          child: Image.asset(
            AppImages.islamicBackground,
            fit: BoxFit.fitWidth,
          ),
        ),
        SizedBox(
            height: screenHeight * .25,
            width: 290,
            child: Image.asset(AppImages.logo))
      ],
    );
  }
}
