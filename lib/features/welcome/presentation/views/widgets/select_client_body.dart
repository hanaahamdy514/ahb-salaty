import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/styles.dart';
import 'package:task1/features/auth/presentation/views/login.dart';
import 'package:task1/features/welcome/presentation/view_model/select_client/select_client_states.dart';
import 'package:task1/features/welcome/presentation/view_model/select_client/selsect_client_cubit.dart';
import 'package:task1/features/welcome/presentation/views/widgets/selsect_client_options.dart';
import '../../../../../core/utils/ widgets/islamic_header.dart';
import '../../../../../generated/l10n.dart';

class SelectClientViewBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const IslamicHeader(),
          SizedBox(
            height: screenHeight * .09,
          ),
          Text(
            S.of(context).select_client,
            style: Styles.textStyle22.copyWith(fontWeight: FontWeight.w600),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: screenHeight * .04,
          ),

          SizedBox(
            height: screenHeight * .1,
          ),
          SizedBox(
            height: screenHeight * .28,
            child: BlocBuilder<SelectClientCubit, SelectClientStates>(
              builder: (context, state) {
                SelectClientCubit cubit = SelectClientCubit.get(context);
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    BlocBuilder<SelectClientCubit, SelectClientStates>(
                        builder: (context, state) {
                      return SelectClientOptions(
                        clientCategory: cubit.clientCategory,
                        onPressed: () {
                          if (kDebugMode) {
                            print(cubit.clientCategory);
                          }
                          cubit.selectClientAction(false);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Login(),
                            ),
                          );
                        },
                        clientLabel: S.of(context).child,
                      );
                    }),
                    SelectClientOptions(
                        onPressed: () {
                          if (kDebugMode) {
                            print(cubit.clientCategory);
                          }
                          cubit.selectClientAction(true);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Login(
                                  // clientCategory: clientCategory,
                                  ),
                            ),
                          );
                        },
                        clientLabel: S.of(context).parent,
                        clientCategory: cubit.clientCategory)
                  ],
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
