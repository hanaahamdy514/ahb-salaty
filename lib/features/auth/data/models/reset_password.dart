class MessageModel{
final String key;
final String message;

  MessageModel({required this.key,required this.message});
  factory MessageModel.FomJson(Map<String,dynamic>json){
    return MessageModel(key: json["key"], message: json["msg"]);
  }
}