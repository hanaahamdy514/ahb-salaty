import 'package:equatable/equatable.dart';

class Child extends Equatable {
  final num? id;
  final String? name;
  final dynamic userName;
  final String? image;
  final String? lang;
  final bool? isNotify;
  final bool? timeFormat;
  final String? token;
  final num? stars;
  final num? wisams;
  final num? prayerPoints;
  final num? quranPoints;
  final num? azkarPoints;
  final num? fatherId;
  final num? roomId;

  const Child({
    this.id,
    this.name,
    this.userName,
    this.image,
    this.lang,
    this.isNotify,
    this.timeFormat,
    this.token,
    this.stars,
    this.wisams,
    this.prayerPoints,
    this.quranPoints,
    this.azkarPoints,
    this.fatherId,
    this.roomId,
  });

  factory Child.fromJson(Map<String, dynamic> json) => Child(
        id: num.tryParse(json['id'].toString()),
        name: json['name']?.toString(),
        userName: json['user_name']?.toString(),
        image: json['image']?.toString(),
        lang: json['lang']?.toString(),
        isNotify: json['is_notify']?.toString().contains("true"),
        timeFormat: json['time_format']?.toString().contains("true"),
        token: json['token']?.toString(),
        stars: num.tryParse(json['stars'].toString()),
        wisams: num.tryParse(json['wisams'].toString()),
        prayerPoints: num.tryParse(json['prayer_points'].toString()),
        quranPoints: num.tryParse(json['quran_points'].toString()),
        azkarPoints: num.tryParse(json['azkar_points'].toString()),
        fatherId: num.tryParse(json['father_id'].toString()),
        roomId: num.tryParse(json['room_id'].toString()),
      );

  Map<String, dynamic> toJson() => {
        if (id != null) 'id': id,
        if (name != null) 'name': name,
        if (userName != null) 'user_name': userName,
        if (image != null) 'image': image,
        if (lang != null) 'lang': lang,
        if (isNotify != null) 'is_notify': isNotify,
        if (timeFormat != null) 'time_format': timeFormat,
        if (token != null) 'token': token,
        if (stars != null) 'stars': stars,
        if (wisams != null) 'wisams': wisams,
        if (prayerPoints != null) 'prayer_points': prayerPoints,
        if (quranPoints != null) 'quran_points': quranPoints,
        if (azkarPoints != null) 'azkar_points': azkarPoints,
        if (fatherId != null) 'father_id': fatherId,
        if (roomId != null) 'room_id': roomId,
      };

  @override
  List<Object?> get props {
    return [
      id,
      name,
      userName,
      image,
      lang,
      isNotify,
      timeFormat,
      token,
      stars,
      wisams,
      prayerPoints,
      quranPoints,
      azkarPoints,
      fatherId,
      roomId,
    ];
  }
}
