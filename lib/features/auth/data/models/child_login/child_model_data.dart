import 'package:equatable/equatable.dart';

import 'child.dart';

class Data extends Equatable {
  final Child? user;

  const Data({this.user});

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        user: json['user'] == null
            ? null
            : Child.fromJson(Map<String, dynamic>.from(json['user'])),
      );

  Map<String, dynamic> toJson() => {
        if (user != null) 'user': user?.toJson(),
      };

  @override
  List<Object?> get props => [user];
}
