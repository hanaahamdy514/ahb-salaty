import 'package:equatable/equatable.dart';

import 'child_model_data.dart';

class ChildModel extends Equatable {
	final String? key;
	final String msg;
	final Data? data;

	const ChildModel({this.key,required this.msg, this.data});

	factory ChildModel.fromJson(Map<String, dynamic> json) => ChildModel(
				key: json['key']?.toString(),
				msg: json['msg'].toString(),
				data: json['data'] == null
						? null
						: Data.fromJson(Map<String, dynamic>.from(json['data']))
			);

	Map<String, dynamic> toJson() => {
				if (key != null) 'key': key,
				if (msg != null) 'msg': msg,
				if (data != null) 'data': data?.toJson(),
			};

	@override
	List<Object?> get props => [key, msg, data];
}
