class UserModel {
  dynamic key;
  dynamic msg;
  dynamic data;

  UserModel({this.key, this.msg, this.data});

  UserModel.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    msg = json['msg'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key'] = this.key;
    data['msg'] = this.msg;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  Father? father;

  Data({this.father});

  Data.fromJson(Map<String, dynamic> json) {
    father =
    json['father'] != null ? new Father.fromJson(json['father']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.father != null) {
      data['father'] = this.father!.toJson();
    }
    return data;
  }
}

class Father {
  dynamic id;
  dynamic name;
  dynamic email;
  dynamic countryCode;
  dynamic phone;
  dynamic fullPhone;
  dynamic image;
  dynamic lang;
  dynamic isNotify;
  dynamic timeFormat;
  dynamic token;

  Father(
      {this.id,
        this.name,
        this.email,
        this.countryCode,
        this.phone,
        this.fullPhone,
        this.image,
        this.lang,
        this.isNotify,
        this.timeFormat,
        this.token});

  Father.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    countryCode = json['country_code'];
    phone = json['phone'];
    fullPhone = json['full_phone'];
    image = json['image'];
    lang = json['lang'];
    isNotify = json['is_notify'];
    timeFormat = json['time_format'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['country_code'] = this.countryCode;
    data['phone'] = this.phone;
    data['full_phone'] = this.fullPhone;
    data['image'] = this.image;
    data['lang'] = this.lang;
    data['is_notify'] = this.isNotify;
    data['time_format'] = this.timeFormat;
    data['token'] = this.token;
    return data;
  }
}
