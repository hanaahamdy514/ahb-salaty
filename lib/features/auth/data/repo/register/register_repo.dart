import 'package:dartz/dartz.dart';
import 'package:task1/core/errors/errors.dart';
import '../../models/father/user.data.dart';
abstract class RegisterRepo {
  Future<Either<Failure, UserModel>> userRegister(
      {required phoneNumber,
      required countryCode,
      required password,
      required passwordConfirmation});
}
