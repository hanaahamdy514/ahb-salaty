import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:task1/core/errors/errors.dart';
import 'package:task1/core/utils/api_constants.dart';
import 'package:task1/core/utils/api_service.dart';
import 'package:task1/features/auth/data/repo/register/register_repo.dart';

import '../../models/father/user.data.dart';



class RegisterRepoImplementation implements RegisterRepo {
  final ApiService apiService;

  RegisterRepoImplementation(this.apiService);

  @override
  Future<Either<Failure, UserModel>> userRegister(
      {required phoneNumber,
      required countryCode,
      required password,
      required passwordConfirmation}) async {
    try {
      var data =
          await apiService.postData(endPoint: ApiConstants.register, data: {
        "country_code": countryCode,
        "phone": phoneNumber,
        "password": password,
        "password_confirmation": passwordConfirmation
      });

      return right((UserModel.fromJson(data)));
    } catch (e) {
      if (e is DioException) {
        return left(ServerError.fromDioException(e));
      }
      return left(ServerError(e.toString()));
    }
  }
}
