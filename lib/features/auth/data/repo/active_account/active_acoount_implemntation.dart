import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:task1/core/errors/errors.dart';
import 'package:task1/core/utils/api_service.dart';
import '../../../../../core/utils/api_constants.dart';
import '../../models/father/user.data.dart';
import 'active_account.dart';

class ActiveAccountImplementation implements ActiveAccountRepos {
  ApiService apiService;

  ActiveAccountImplementation(this.apiService);

  @override
  Future<Either<Failure, UserModel>> activeAccount(
      {required activationCode,
      required countryCode,
      required phoneNumber,}) async {
    try {
      var response = await apiService.postData(
          query: {"_method": "patch"},
          endPoint: ApiConstants.activationCode,
          data: {
            "code": activationCode,
            "country_code": countryCode,
            "phone": phoneNumber,
            "device_id": 111,
            "device_type": "android"
          });
      return right(UserModel.fromJson(response));
    } catch (e) {
      if (e is DioException) {
        return left(ServerError.fromDioException(e));
      }
      return left(ServerError(e.toString()));
    }
  }
}
