import 'package:dartz/dartz.dart';
import 'package:task1/core/errors/errors.dart';
import 'package:task1/features/auth/data/models/father/user.data.dart';


abstract class ActiveAccountRepos {
  Future<Either<Failure, UserModel>> activeAccount(
      {required activationCode,
      required countryCode,
      required phoneNumber,
      });
}
