import 'package:dartz/dartz.dart';
import 'package:task1/core/errors/errors.dart';
import 'package:task1/features/auth/data/models/father/user.data.dart';
import 'package:task1/features/auth/data/models/reset_password.dart';

abstract class ResetPasswordRepo {
  Future<Either<Failure, MessageModel>> resendCode({required phoneNumber,required countryCode});
  Future<Either<Failure, MessageModel>> resetPassword({required code, required password,required phone,required countryCode});

}
