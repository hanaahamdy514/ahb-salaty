import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:task1/core/errors/errors.dart';
import 'package:task1/core/utils/api_service.dart';
import 'package:task1/features/auth/data/models/father/user.data.dart';
import 'package:task1/features/auth/data/models/reset_password.dart';
import 'package:task1/features/auth/data/repo/reset_password/reset_password_repo.dart';

import '../../../../../core/utils/api_constants.dart';
import '../../../../../core/utils/constants.dart';

class ResetPasswordRepoImplementation implements ResetPasswordRepo {
  final ApiService apiService;

  ResetPasswordRepoImplementation(this.apiService);

  @override
  Future<Either<Failure, MessageModel>> resendCode(
      {required phoneNumber, required countryCode,}) async {
    try {
      var data = await apiService.get(
          endPoint: ApiConstants.resendCode,
          quary: {"country_code": countryCode, "phone": phoneNumber});

      return right((MessageModel.FomJson(data)));
    } catch (e) {
      if (e is DioException) {
        return left(ServerError.fromDioException(e));
      }
      return left(ServerError(e.toString()));
    }
  }

  @override
  Future<Either<Failure, MessageModel>> resetPassword(
      {required code,
      required password,
      required phone,
      required countryCode}) async {
    try {
      var data = await apiService.postData(
          endPoint: ApiConstants.resetPassword,
          data: {
            "code": code,
            "phone": phone,
            "country_code": countryCode,
            "password": password
          });

      return right((MessageModel.FomJson(data)));
    } catch (e) {
      if (e is DioException) {
        return left(ServerError.fromDioException(e));
      }
      return left(ServerError(e.toString()));
    }
  }
}
