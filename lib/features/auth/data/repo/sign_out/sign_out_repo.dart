import 'package:dartz/dartz.dart';
import 'package:task1/features/auth/data/models/child_login/child_model.dart';

import '../../../../../core/errors/errors.dart';
import '../../models/father/user.data.dart';

abstract class SignOutRepo {
  Future<Either<Failure, UserModel>> parentSignOut();
  Future<Either<Failure, ChildModel>> childSignOut();
}