import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/errors/errors.dart';
import 'package:task1/core/utils/api_constants.dart';
import 'package:task1/core/utils/api_service.dart';
import 'package:task1/features/auth/data/models/child_login/child_model.dart';
import 'package:task1/features/auth/data/models/father/user.data.dart';
import 'package:task1/features/auth/data/repo/sign_out/sign_out_repo.dart';
import 'package:task1/features/auth/presentation/view_model/sign_out/sign_out_cubit.dart';
import 'package:task1/features/welcome/presentation/view_model/select_client/selsect_client_cubit.dart';

class SignOutRepoImplementation implements SignOutRepo {
  final ApiService apiService;

  SignOutRepoImplementation(this.apiService);

  @override
  Future<Either<Failure, UserModel>> parentSignOut() async {
    try {
      var data = await apiService.deleteData(
          endPoint: ApiConstants.fatherSignOut);


      return right((UserModel.fromJson(data)));
    } catch (e) {
      if (e is DioException) {
        return left(ServerError.fromDioException(e));
      }
      return left(ServerError(e.toString()));
    }
  }

  @override
  Future<Either<Failure, ChildModel>> childSignOut()async {
    try {
      var data = await apiService.deleteData(
          endPoint: ApiConstants.sonSignOut);


      return right((ChildModel.fromJson(data)));
    } catch (e) {
      if (e is DioException) {
        return left(ServerError.fromDioException(e));
      }
      return left(ServerError(e.toString()));
    }
  }
}
