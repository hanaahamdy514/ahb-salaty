import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:task1/core/errors/errors.dart';
import 'package:task1/core/utils/api_constants.dart';
import 'package:task1/core/utils/api_service.dart';
import 'package:task1/features/auth/data/models/father/user.data.dart';

import '../../models/child_login/child_model.dart';
import 'login_repo.dart';

class LoginRepoImplementation implements LoginRepo {
  final ApiService apiService;

  LoginRepoImplementation(this.apiService);

  @override
  Future<Either<Failure, UserModel>> parentLogin(
      {required countryCode, required phoneNumber, required password}) async {
    try {
      var response =
          await apiService.postData(endPoint: ApiConstants.parentLogin, data: {
        "country_code": countryCode,
        "phone": phoneNumber,
        "password": password,
        "device_id": 123,
        "device_type": "android"
      });
      return right(UserModel.fromJson(response));
    } catch (e) {
      if (e is DioException) {
        return left(ServerError.fromDioException(e));
      }
      return left(ServerError(e.toString()));
    }
  }

  @override
  Future<Either<Failure, ChildModel>> childLogin(
      {required userName, required password}) async {
    try {
      var response =
          await apiService.postData(endPoint: ApiConstants.childLogin, data: {
        "user_name": userName,
        "password": password,
        "device_id": 123,
        "device_type": "android"
      });
      return right(ChildModel.fromJson(response));
    } catch (e) {
      if (e is DioException) {
        return left(ServerError.fromDioException(e));
      }
      return left(ServerError(e.toString()));
    }
  }
}
