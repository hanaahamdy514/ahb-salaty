import 'package:dartz/dartz.dart';
import 'package:task1/core/errors/errors.dart';
import 'package:task1/features/auth/data/models/child_login/child_model.dart';

import '../../models/father/user.data.dart';

abstract class LoginRepo {
  Future<Either<Failure, UserModel>> parentLogin(
      {required dynamic countryCode, required phoneNumber, required password});

  Future<Either<Failure, ChildModel>> childLogin(
      {required userName, required password});
}
