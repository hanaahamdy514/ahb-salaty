import 'package:flutter/material.dart';
import 'package:task1/core/utils/styles.dart';
import 'package:task1/generated/l10n.dart';
class RegisterFirstSection extends StatelessWidget {
  const RegisterFirstSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    return Column(
      children: [
         Text(
          S.of(context).sign_up,
          style: Styles.textStyle20,
        ),
        const SizedBox(
          height: 16,
        ),

        SizedBox(
          height: screenHeight * .03,
        ),
      ],
    );
  }
}
