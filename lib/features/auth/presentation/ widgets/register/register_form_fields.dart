import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/custom_form_field.dart';
import 'package:task1/core/utils/%20widgets/drop_down_button.dart';
import 'package:task1/core/utils/%20widgets/toast.dart';
import 'package:task1/features/auth/presentation/%20widgets/register/register_button.dart';
import 'package:task1/features/auth/presentation/%20widgets/register/register_first_section.dart';
import 'package:task1/features/auth/presentation/view_model/regiter/register_cubit.dart';
import 'package:task1/features/auth/presentation/view_model/regiter/register_states.dart';
import 'package:task1/generated/l10n.dart';
class RegisterFormFields extends StatelessWidget {
 const RegisterFormFields({Key? key, required this.formKey}) : super(key: key);
  final GlobalKey<FormState> formKey;

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: screenHeight * .04),
      child: Column(
        children: [
          const RegisterFirstSection(),
          BlocConsumer<RegisterCubit, RegisterStats>(
            listener: (context, state) {
              if (state is RegisterSuccessState) {
                if (state.userModel.msg == "success") {
                  toast(message: state.userModel.msg, color: Colors.green);
                  GoRouter.of(context).push('/activeAccount');
                } else if (state.userModel.key == "fail") {
                  toast(message: state.userModel.msg, color: Colors.green);
                }
              } else if (state is RegisterErrorState) {
                toast(
                  color: Colors.red,
                  message: state.errorMessage,
                );
              }
            },
            builder: (context, state) {
              RegisterCubit cubit = RegisterCubit.get(context);
              return Column(
                children: [
                  CustomFormField(
                    suffix: dropDownItem(cubit),
                    hint: S.of(context).phone,
                    controller: cubit.phoneController,
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(
                    height: screenHeight * .01,
                  ),
                  CustomFormField(
                   suffix: IconButton(
                      onPressed: () {
                        cubit.iconVisibility();
                      },
                      icon: Icon(
                        cubit.visibility,
                        size: 16,
                      ),
                    ),
                    hint: S.of(context).password,
                    isVisible: cubit.isVisible,
                    controller: cubit.passwordController,
                  ),
                  SizedBox(
                    height: screenHeight * .01,
                  ),
                  CustomFormField(
                    suffix: IconButton(
                      onPressed: () {
                        cubit.iconVisibility();
                      },
                      icon: Icon(
                        cubit.visibility,
                        size: 16,
                      ),
                    ),
                    hint: S.of(context).confirm_password,
                    isVisible: cubit.isVisible,
                    controller: cubit.reEnterPasswordController,
                  ),
                  SizedBox(
                    height: screenHeight * .02,
                  ),
                  RegisterButton(state: state, cubit: cubit, formKey: formKey)
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}
