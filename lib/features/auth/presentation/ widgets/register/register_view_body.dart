import "package:dio/dio.dart";
import "package:flutter/material.dart";
import "package:flutter_bloc/flutter_bloc.dart";
import "package:go_router/go_router.dart";
import "package:task1/core/utils/service_locator.dart";
import 'package:task1/features/auth/presentation/%20widgets/register/register_form_fields.dart';
import "package:task1/features/auth/presentation/view_model/regiter/register_cubit.dart";
import "package:task1/generated/l10n.dart";
import '../../../../../core/utils/ widgets/islamic_header.dart';
import '../../../../../core/utils/app_colors.dart';
import "../../../data/repo/register/register_repo_implementation.dart";

class RegisterVieWBody extends StatelessWidget {
  const RegisterVieWBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return BlocProvider(
      create: (BuildContext context) =>
          RegisterCubit(getIt.get<RegisterRepoImplementation>()),
      child: Form(
        key: formKey,
        autovalidateMode: AutovalidateMode.always,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          child: Column(
            children: [
              const IslamicHeader(),
              RegisterFormFields(formKey: formKey),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    S.of(context).do_you_have_an_account,
                  ),
                  TextButton(
                      onPressed: () {
                        GoRouter.of(context).pop();
                      },
                      style: TextButton.styleFrom(
                          foregroundColor: AppColors.primaryColor),
                      child:  Text(
                      S.of(context).login,
                      )),

                ],
              ),
              SizedBox(
                height: screenHeight * .02,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
