import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';

import '../../../../../core/utils/ widgets/default_button.dart';
import '../../../../../core/utils/local_db.dart';
import '../../../../../generated/l10n.dart';
import '../../view_model/regiter/register_cubit.dart';
import '../../view_model/regiter/register_states.dart';

class RegisterButton extends StatelessWidget {
  const RegisterButton(
      {Key? key,
      required this.state,
      required this.cubit,
      required this.formKey})
      : super(key: key);
  final RegisterStats state;
  final RegisterCubit cubit;
  final GlobalKey<FormState> formKey;

  @override
  Widget build(BuildContext context) {
    return ConditionalBuilder(
        condition: state is! RegisterLoadingState,
        builder: (context) {
          return DefaultButton(
              buttonLabel:S.of(context).sign_up,
              onPressed: () async {
                if (formKey.currentState!.validate()) ;
                await LocalHelper.setData(
                    key: "phone", value: cubit.phoneController.text);
                cubit.userRegister(
                    phoneNumber: cubit.phoneController.text,
                    countryCode: cubit.selectedCountryCode,
                    password: cubit.passwordController.text,
                    passwordConfirmation: cubit.reEnterPasswordController.text);
                cubit.registeresNumber = cubit.phoneController.text;
              });
        },
        fallback: (context) {
          return const Center(child: CircularProgressIndicator());
        });
  }
}
