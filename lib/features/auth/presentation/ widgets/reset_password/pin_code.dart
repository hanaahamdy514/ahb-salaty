import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:task1/core/utils/app_colors.dart';


class PinCode extends StatelessWidget {
  PinCode({Key? key,required this.controller}) : super(key: key);
TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return PinCodeTextField(
      controller:controller ,
      validator: (value) {
        if (value?.isEmpty ?? true) {
          return "من فضلك ادخل الكود";
        }
        return null;
      },
      appContext: context,
      length: 4,
      obscureText: false,
 separatorBuilder:(context,index)=>const SizedBox(width: 10,),
      cursorColor: AppColors.primaryColor,
      animationType: AnimationType.fade,
      keyboardType: TextInputType.number,
      pinTheme: PinTheme(
        inactiveFillColor: Colors.white,
        shape: PinCodeFieldShape.box,
        selectedFillColor: AppColors.white,
        activeColor: AppColors.white,
        inactiveColor: AppColors.white,
        borderRadius: BorderRadius.circular(5),
        // selectedColor: Colors.white,
        fieldHeight: 60,
        fieldWidth: 40,
        activeFillColor: AppColors.lightBlue,
      ),
      animationDuration:const Duration(milliseconds: 300),
      backgroundColor: Colors.transparent,
      enableActiveFill: true,
      onCompleted: (v) {},
      onChanged: (value) {
        print(value);
      },
    );
  }
}
