import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/%20widgets/custom_form_field.dart';
import 'package:task1/core/utils/styles.dart';
import 'package:task1/features/auth/presentation/%20widgets/reset_password/pin_code.dart';
import 'package:task1/features/auth/presentation/view_model/reset_password/reset_password_cubit.dart';
import 'package:task1/features/auth/presentation/view_model/reset_password/reset_password_states.dart';
import 'package:task1/generated/l10n.dart';

import '../../../../../core/utils/app_colors.dart';

class ResetPssWordFormFields extends StatelessWidget {
  const ResetPssWordFormFields({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ResetPasswordCubit, ResetPasswordStates>(
        builder: (context, state) {
      ResetPasswordCubit cubit = ResetPasswordCubit.get(context);
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          PinCode(
            controller: cubit.resetCodeController,
          ),
          Text(
           S.of(context).password,
            style: Styles.textStyle16.copyWith(color: AppColors.darkBlack),

          ),
          const SizedBox(
            height: 8,
          ),
          CustomFormField(
            isVisible: cubit.isVisible,
           suffix: IconButton(
              onPressed: () {
                cubit.iconVisibility();
              },
              icon: Icon(
                cubit.visibility,
                size: 16,
              ),
            ),
            hint: S.of(context).password,
            controller: cubit.passWordController,
          ),
          const SizedBox(
            height: 8,

          )
        ],
      );
    });
  }
}
