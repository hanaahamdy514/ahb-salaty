import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/circular_indicator.dart';
import 'package:task1/core/utils/%20widgets/toast.dart';
import 'package:task1/features/auth/presentation/view_model/reset_password/reset_password_states.dart';
import 'package:task1/generated/l10n.dart';

import '../../../../../core/utils/ widgets/default_button.dart';
import '../../../../../core/utils/constants.dart';
import '../../../../../core/utils/local_db.dart';
import '../../view_model/reset_password/reset_password_cubit.dart';

class ResetPasswordButton extends StatelessWidget {
  const ResetPasswordButton({Key? key, required this.formKey})
      : super(key: key);
  final GlobalKey<FormState> formKey;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ResetPasswordCubit, ResetPasswordStates>(
      listener: (context, state) {
        if (state is ResetPasswordSuccessState) {
          print(state.messageModel.message);
          if (state.messageModel.key == "success") {
            debugPrint(state.messageModel.message);
            toast(message: state.messageModel.message, color: Colors.green);
            GoRouter.of(context).pushReplacement("/Login");
          } else {
            toast(message: state.messageModel.message, color: Colors.red);
          }
        } else if (state is ResetPasswordErrorState) {
          toast(message: state.erroeMessage, color: Colors.red);
        }
      },
      builder: (context, state) {
        ResetPasswordCubit cubit = ResetPasswordCubit.get(context);
        return ConditionalBuilder(
            condition: state is! ResetPasswordLoadingState,
            builder: (context) {
              return DefaultButton(
                buttonLabel: S.of(context).confirm,
                onPressed: () async {
                  if (formKey.currentState!.validate()) {
                    phoneforResetPassword = await LocalHelper.getData(
                        key: "phone_to_reset_password");
                    selected_country = await LocalHelper.getData(
                      key: "country_code",
                    );

                    cubit.resetPassword(

                        password: cubit.passWordController.text,
                        code: cubit.resetCodeController.text,
                        country_code: selected_country,
                        phone: phoneforResetPassword);
                  }
                },
              );
            },
            fallback: (context) => const CircularIndicator());
      },
    );
  }
}
