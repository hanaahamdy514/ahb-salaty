import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/toast.dart';
import 'package:task1/core/utils/constants.dart';
import 'package:task1/core/utils/local_db.dart';
import 'package:task1/generated/l10n.dart';

import '../../../../../core/utils/ widgets/circular_indicator.dart';
import '../../../../../core/utils/ widgets/custom_form_field.dart';
import '../../../../../core/utils/ widgets/default_button.dart';
import '../../../../../core/utils/ widgets/drop_down_button.dart';
import '../../view_model/regiter/register_cubit.dart';
import '../../view_model/reset_password/resend_code_cubit.dart';
import '../../view_model/reset_password/resend_code_states.dart';

class ResendCodeButton extends StatelessWidget {
  ResendCodeButton({Key? key, required this.formKey}) : super(key: key);

  final GlobalKey<FormState> formKey;

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return BlocConsumer<ResendCodeCubit, ResendCodeStates>(
      listener: (context, state) {
        if (state is ResendCodeSuccessState) {
          if (state.messageModel.key == "success") {
            debugPrint(state.messageModel.key);
            debugPrint(state.messageModel.message);
            toast(message: state.messageModel.message, color: Colors.green);
            GoRouter.of(context).push("/resetPassword");
          } else {
            toast(message: state.messageModel.message, color: Colors.red);
          }
        } else if (state is ResendCodeErrorState) {
          toast(message: state.erroeMessage, color: Colors.red);
        }
      },
      builder: (context, state) {
        ResendCodeCubit cubit = ResendCodeCubit.get(context);
        return Column(
          children: [
            CustomFormField(
                suffix: dropDownItem(RegisterCubit.get(context)),
                keyboardType: TextInputType.number,
                hint: S.of(context).phone,
                controller: cubit.phoneNumberToResetPassWord),
            SizedBox(
              height: screenHeight * .03,
            ),
            ConditionalBuilder(
                condition: state is! ResendCodeLoadingState,
                builder: (context) => DefaultButton(
                      buttonLabel: S.of(context).confirm,
                      onPressed: () async {
                        if (formKey.currentState!.validate()) {
                          await LocalHelper.setData(
                              key: "phone_to_reset_password",
                              value: cubit.phoneNumberToResetPassWord.text);
                          await LocalHelper.setData(
                              key: "country_code",
                              value: cubit.selectedCountryCode);
                          cubit.resendPassword(
                              phoneNumber:
                                  cubit.phoneNumberToResetPassWord.text,
                              countryCode: cubit.selectedCountryCode);
                        }
                      },
                    ),
                fallback: (context) => const CircularIndicator()),
          ],
        );
      },
    );
  }
}
