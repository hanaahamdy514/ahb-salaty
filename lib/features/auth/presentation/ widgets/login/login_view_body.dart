import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/features/auth/presentation/%20widgets/login/Login_question.dart';
import 'package:task1/features/auth/presentation/%20widgets/login/login_divider.dart';
import 'package:task1/features/auth/presentation/%20widgets/login/login_form_fields.dart';
import 'package:task1/features/welcome/presentation/view_model/select_client/selsect_client_cubit.dart';
import '../../../../../core/utils/ widgets/islamic_header.dart';


class LoginViewBody extends StatelessWidget {
  LoginViewBody({Key? key, this.clientCategory}) : super(key: key);

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final bool? clientCategory;

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    return Form(
      autovalidateMode: AutovalidateMode.always,
      key: formKey,
      child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        child: Column(
          children: [
            const IslamicHeader(),
            LoginFormFields(
              formKey: formKey,
            ),
            BlocProvider.of<SelectClientCubit>(context).clientCategory
                ? Column(
                    children: [
                      const LoginDivider(),
                      const LoginQuestion(),
                      SizedBox(
                        height: screenHeight * .02,
                      ),
                    ],
                  )
                : const SizedBox()
          ],
        ),
      ),
    );
  }
}
