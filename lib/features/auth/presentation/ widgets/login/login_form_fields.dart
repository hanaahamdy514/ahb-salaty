import 'dart:developer';

import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/custom_form_field.dart';
import 'package:task1/core/utils/%20widgets/forget_passord.dart';
import 'package:task1/core/utils/%20widgets/toast.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/core/utils/constants.dart';
import 'package:task1/core/utils/local_db.dart';
import 'package:task1/core/utils/styles.dart';
import 'package:task1/features/auth/presentation/%20widgets/login/login_button.dart';
import 'package:task1/features/auth/presentation/%20widgets/login/login_first_section.dart';
import 'package:task1/features/auth/presentation/view_model/login/login_cubit.dart';
import 'package:task1/features/auth/presentation/view_model/login/login_states.dart';
import 'package:task1/generated/l10n.dart';
import '../../../../../core/utils/ widgets/drop_down_button.dart';
import '../../../../welcome/presentation/view_model/select_client/selsect_client_cubit.dart';

class LoginFormFields extends StatelessWidget {
  const LoginFormFields({Key? key, required this.formKey}) : super(key: key);

  final GlobalKey<FormState> formKey;

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: screenHeight * .04),
      child: Column(
        children: [
          const LoginFirstSection(),
          BlocConsumer<LoginCubit, LoginStates>(
            listener: (context, state) async {
              if (state is ChildLoginSuccessState) {
                if (state.userData.key == "success") {
                  toast(message: state.userData.msg, color: Colors.green);

                  await LocalHelper.setData(
                      key: "son_token",
                      value: state.userData.data?.user?.token);
                  GoRouter.of(context).pushReplacement("/home");
                } else if (state.userData.key == "fail") {
                  toast(message: state.userData.msg, color: Colors.red);
                }
              } else if (state is ChildLoginErrorState) {
                toast(message: state.errorMessage, color: Colors.red);
              }
//*********************************parent******************************************//
              else if (state is ParentLoginSuccessState) {
                log('testtttt ${state.userModel.data?.father?.token.toString()}');
                if (state.userModel.key == "success") {
                  await LocalHelper.setData(
                      key: "father_token",
                      value: state.userModel.data?.father?.token).then((value) => log("success")).catchError((onError){
                        log("error in caching ${onError.toString()}");
                  });

                  toast(message: state.userModel.msg, color: Colors.green);
                  GoRouter.of(context).push("/home");
                } else if (state.userModel.key == "fail") {
                  toast(message: state.userModel.msg, color: Colors.red);
                }
              } else if (state is LoginErrorState) {
                toast(message: state.errorMessage, color: Colors.red);
              }
            },
            builder: (context, state) {
              SelectClientCubit client = SelectClientCubit.get(context);
              LoginCubit cubit = LoginCubit.get(context);
              return Column(
                children: [
                  CustomFormField(
                    suffix: client.clientCategory == true
                        ? dropDownItem(cubit)
                        : null,
                    hint: client.clientCategory == false
                        ? S.of(context).user_name
                        : S.of(context).phone,
                    controller: cubit.phoneController,
                  ),
                  CustomFormField(
                      suffix: IconButton(
                        onPressed: () {
                          cubit.iconVisibility();
                        },
                        icon: Icon(
                          cubit.visibility,
                          size: 16,
                        ),
                      ),
                      hint: S.of(context).password,
                      controller: cubit.passwordController,
                      isVisible: cubit.isVisible),
                  ForgetPasswordButton(
                    buttonAction: () {
                      GoRouter.of(context).push('/enterPhoneToResetPassword');
                    },
                  ),
                  SizedBox(
                    height: screenHeight * .02,
                  ),
                  LoginButton(
                      state: state,
                      client: client,
                      cubit: cubit,
                      formKey: formKey),
                  SizedBox(
                    height: screenHeight * .02,
                  )
                ],
              );
            },
          ),
        ],
      ),
    );
  }
}
