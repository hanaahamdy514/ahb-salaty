import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/generated/l10n.dart';

import '../../../../../core/utils/app_colors.dart';

class LoginQuestion extends StatelessWidget {
  const LoginQuestion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
            S.of(context).dont_haveanaccount
        ),
        TextButton(
          onPressed: () {
            GoRouter.of(context).push("/register");
          },
          style: TextButton.styleFrom(foregroundColor: AppColors.primaryColor),
          child: Text(
            S.of(context).create_account,
          ),
        ),

      ],
    );
  }
}
