import 'package:flutter/material.dart';
import 'package:task1/core/utils/styles.dart';

import '../../../../../generated/l10n.dart';

class LoginDivider extends StatelessWidget {
  const LoginDivider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: screenWidth * .12),
      child:  Row(
        children: [
          const Expanded(
              child: Divider(endIndent: 10,
            thickness: 2,
          )),
          Text(S.of(context).or,style: Styles.textStyle14,),
          const Expanded(

            child: Divider(indent: 10,
              thickness: 1,
            ),
          )
        ],
      ),
    );
  }
}
