import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:task1/core/utils/%20widgets/circular_indicator.dart';

import 'package:task1/features/auth/presentation/view_model/login/login_cubit.dart';
import 'package:task1/features/auth/presentation/view_model/login/login_states.dart';
import 'package:task1/features/welcome/presentation/view_model/select_client/selsect_client_cubit.dart';
import 'package:task1/generated/l10n.dart';

import '../../../../../core/utils/ widgets/default_button.dart';

class LoginButton extends StatelessWidget {
  const LoginButton(
      {Key? key,
      required this.state,
      required this.client,
      required this.cubit,
      required this.formKey})
      : super(key: key);
  final LoginStates state;
  final SelectClientCubit client;
  final LoginCubit cubit;
  final GlobalKey<FormState> formKey;

  @override
  Widget build(BuildContext context) {
    return ConditionalBuilder(
        condition: state is! LoginLoadingState,
        builder: (context) => DefaultButton(
            buttonLabel: S.of(context).login,
            onPressed: () {

              if (formKey.currentState!.validate()) {
                if (client.clientCategory == true) {
                  cubit.parentLogin(
                      countryCode: cubit.selectedCountryCode,
                      phoneNumber: cubit.phoneController.text,
                      password: cubit.passwordController.text);
                }
                else if (client.clientCategory == false) {
                  cubit.childLogin(
                      userName: cubit.phoneController.text,
                      password: cubit.passwordController.text);
                }
              }

            }),
        fallback: (context) {
          return const CircularIndicator();
        });
  }
}
