import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:task1/core/utils/local_db.dart';

import '../../../../../core/utils/ widgets/default_button.dart';
import '../../../../../core/utils/constants.dart';
import '../../view_model/active_account/active_code.dart';
import '../../view_model/active_account/active_states.dart';

class ActiveAccountButton extends StatelessWidget {
  const ActiveAccountButton(
      {Key? key,
      required this.state,
      required this.cubit,
      required this.formKey,
      required this.activateController})
      : super(key: key);
  final ActiveAccountStates state;
  final ActiveAccountCubit cubit;
  final GlobalKey<FormState> formKey;
  final TextEditingController activateController;

  @override
  Widget build(BuildContext context) {
    return ConditionalBuilder(
        condition: state is! ActiveAccountLoadingState,
        builder: (context) {
          return DefaultButton(
              onPressed: () async {
                if (formKey.currentState!.validate()) {
                  register_phone = await LocalHelper.getData(key: "phone");
                  cubit.activateAccount(
                      activationCode: activateController.text,
                      phoneNumber: register_phone,
                      countryCode: 20);
                }
              },
              buttonLabel: "تاكيد");
        },
        fallback: (context) {
          return const CircularProgressIndicator();
        });
  }
}
