import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/service_locator.dart';
import 'package:task1/features/auth/data/repo/login/login_repo_implementtion.dart';
import 'package:task1/features/auth/presentation/%20widgets/login/login_view_body.dart';
import 'package:task1/features/auth/presentation/view_model/login/login_cubit.dart';

class Login extends StatelessWidget {
  Login({
    Key? key,
  }) : super(key: key);
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => LoginCubit(getIt.get<LoginRepoImplementation>()),
        child: Scaffold(body: LoginViewBody()));
  }
}
