import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/islamic_header.dart';
import 'package:task1/core/utils/%20widgets/toast.dart';
import 'package:task1/core/utils/service_locator.dart';
import 'package:task1/core/utils/styles.dart';
import 'package:task1/features/auth/presentation/%20widgets/activate_account/activate_accont_button.dart';
import 'package:task1/features/auth/presentation/view_model/active_account/active_code.dart';
import 'package:task1/features/auth/presentation/view_model/active_account/active_states.dart';

import '../ widgets/reset_password/pin_code.dart';
import '../../data/repo/active_account/active_acoount_implemntation.dart';

class ActiveAccount extends StatelessWidget {
  ActiveAccount({Key? key}) : super(key: key);
  TextEditingController activeController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return BlocProvider(
      create: (context) =>
          ActiveAccountCubit(getIt.get<ActiveAccountImplementation>()),
      child: Scaffold(
        body: Form(
          key: formKey,
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: Column(
              children: [
                const IslamicHeader(),
                const Text(
                  "ادخل كود التفعيل",
                  style: Styles.textStyle20,
                ),
                SizedBox(
                  height: screenHeight * .1,
                ),
                BlocConsumer<ActiveAccountCubit, ActiveAccountStates>(
                  listener: (context, state) {
                    if (state is ActiveAccountSuccessState) {
                      if (state.userData.key == "success") {
                        toast(message: state.userData.msg, color: Colors.green);
                        GoRouter.of(context).pushReplacement("/Login");
                      } else {
                        toast(message: state.userData.msg, color: Colors.red);
                      }
                    } else if (state is ActiveAccountErrorState) {
                      toast(message: state.errorMessage, color: Colors.red);
                    }
                  },
                  builder: (context, state) {
                    ActiveAccountCubit cubit = ActiveAccountCubit.get(context);
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      child: Column(
                        children: [
                          PinCode(controller: cubit.activateCodeController),
                          SizedBox(
                            height: screenHeight * .06,
                          ),
                          ActiveAccountButton(
                              state: state,
                              cubit: cubit,
                              formKey: formKey,
                              activateController: cubit.activateCodeController)
                        ],
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
