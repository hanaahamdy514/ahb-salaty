import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/%20widgets/default_button.dart';
import 'package:task1/core/utils/%20widgets/islamic_header.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/core/utils/service_locator.dart';
import 'package:task1/core/utils/styles.dart';
import 'package:task1/features/auth/presentation/%20widgets/reset_password/reset_password_button.dart';
import 'package:task1/features/auth/presentation/view_model/reset_password/reset_password_cubit.dart';
import 'package:task1/features/auth/presentation/view_model/reset_password/reset_password_states.dart';
import '../ widgets/reset_password/reset_password_form_fields.dart';
import '../../../../generated/l10n.dart';
import '../../data/repo/reset_password/reset_password_implemntation.dart';

class ResetPassWord extends StatelessWidget {
  ResetPassWord({Key? key}) : super(key: key);
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return BlocProvider(
      create: (context) => ResetPasswordCubit(
        getIt.get<ResetPasswordRepoImplementation>(),
      ),
      child: BlocBuilder<ResetPasswordCubit, ResetPasswordStates>(
        builder: (context, state) {
          return Scaffold(
            body: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              child: Form(
                autovalidateMode: AutovalidateMode.always,
                key: formKey,
                child: Column(
                  children: [
                    const IslamicHeader(),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: screenHeight * .04),
                      child: Column(
                        children: [
                          Text(
                            S.of(context).set_new_password,
                            style: Styles.textStyle20,
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            S.of(context).set_new_password_hint,
                            textAlign: TextAlign.center,
                            style: Styles.textStyle16
                                .copyWith(color: AppColors.lightBlack),
                          ),
                          SizedBox(
                            height: screenHeight * .03,
                          ),
                          const ResetPssWordFormFields(),
                          const SizedBox(
                            height: 20,
                          ),
                          ResetPasswordButton(
                            formKey: formKey,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
