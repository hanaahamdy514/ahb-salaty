import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/%20widgets/islamic_header.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/core/utils/service_locator.dart';
import 'package:task1/core/utils/styles.dart';
import 'package:task1/features/auth/presentation/view_model/reset_password/resend_code_cubit.dart';

import '../ widgets/reset_password/resend_code_button.dart';
import '../../../../generated/l10n.dart';
import '../../data/repo/reset_password/reset_password_implemntation.dart';

class PhoneNumberToResetPassword extends StatelessWidget {
  PhoneNumberToResetPassword({Key? key}) : super(key: key);
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController phoneNumberToResetPassWord = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;

    return BlocProvider(
      create: (context) =>
          ResendCodeCubit(getIt.get<ResetPasswordRepoImplementation>()),
      child: Scaffold(
        body: Form(
          key: formKey,
          autovalidateMode: AutovalidateMode.always,
          child: SingleChildScrollView(
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            physics: const BouncingScrollPhysics(),
            child: Column(
              children: [
                const IslamicHeader(),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: screenWidth * .06),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        S.of(context).reset_password,
                        style: Styles.textStyle20,
textAlign: TextAlign.center,
                      ),
                      SizedBox(height: screenHeight*.02,),
                      Text(
                        S.of(context).reset_password_hint,
                        textAlign: TextAlign.center,
                        style: Styles.textStyle16
                            .copyWith(color: AppColors.lightBlack),
                      ),
                      SizedBox(
                        height: screenHeight * .02,
                      ),
                      Text(
                        S.of(context).phone,
                        style: Styles.textStyle16
                            .copyWith(color: AppColors.darkBlack),
                      ),
                      SizedBox(
                        height: screenHeight * .02,
                      ),
                      ResendCodeButton(
                        formKey: formKey,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
