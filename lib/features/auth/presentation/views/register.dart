import 'package:flutter/material.dart';
import 'package:task1/features/auth/presentation/%20widgets/register/register_view_body.dart';

class Register extends StatelessWidget {
  const Register({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: RegisterVieWBody(),
    );
  }
}
