import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/features/auth/data/repo/reset_password/reset_password_repo.dart';
import 'package:task1/features/auth/presentation/view_model/reset_password/reset_password_states.dart';

class ResetPasswordCubit extends Cubit<ResetPasswordStates> {
  final ResetPasswordRepo resetPasswordRepo;

  ResetPasswordCubit(this.resetPasswordRepo) : super(ResetPasswordInitState());

  static ResetPasswordCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  TextEditingController passWordController = TextEditingController();
  TextEditingController resetCodeController = TextEditingController();
  bool isVisible = true;
  IconData visibility = Icons.visibility_off;

  void iconVisibility() {
    isVisible = !isVisible;
    visibility = isVisible ? Icons.visibility_off : Icons.visibility;
    emit(ResetPasswordVisibilityState());
  }

  Future resetPassword({required password, required code,required country_code,required phone }) async {
    emit(ResetPasswordLoadingState());
    var response =
        await resetPasswordRepo.resetPassword(code: code, password: password,countryCode: country_code, phone: phone,);

    response.fold((failure) {
      emit(ResetPasswordErrorState(failure.massege));
    }, (userData) {
      emit(ResetPasswordSuccessState(messageModel: userData));
    });
  }
}
