import 'package:task1/features/auth/data/models/reset_password.dart';

abstract class ResetPasswordStates {}

class ResetPasswordInitState extends ResetPasswordStates {}

class ResetPasswordVisibilityState extends ResetPasswordStates {}
class ResetPasswordLoadingState extends ResetPasswordStates {}


class ResetPasswordSuccessState extends ResetPasswordStates{
  final MessageModel messageModel;

  ResetPasswordSuccessState({required this.messageModel});

}
class ResetPasswordErrorState extends ResetPasswordStates{
  final String erroeMessage;

  ResetPasswordErrorState(this.erroeMessage);

}
