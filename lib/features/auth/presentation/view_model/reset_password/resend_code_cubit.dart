import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/features/auth/presentation/view_model/reset_password/resend_code_states.dart';

import '../../../data/repo/reset_password/reset_password_repo.dart';

class ResendCodeCubit extends Cubit<ResendCodeStates> {
  final ResetPasswordRepo resetPasswordRepo;

  ResendCodeCubit(this.resetPasswordRepo) : super(ResendCodeInitState());
  TextEditingController phoneNumberToResetPassWord = TextEditingController();

  static ResendCodeCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  var selectedCountryCode = 20;

  changeSelectedCountryCode(value) {
    selectedCountryCode = value;
    emit(ResendCodSelectCountryState());
  }

  Future resendPassword({required phoneNumber, required countryCode}) async {
    emit(ResendCodeLoadingState());
    var response = await resetPasswordRepo.resendCode(
        phoneNumber: phoneNumber, countryCode: countryCode);

    response.fold((failure) {
      emit(ResendCodeErrorState(failure.massege));
    }, (userData) {
      emit(ResendCodeSuccessState(messageModel: userData));
    });
  }
}
