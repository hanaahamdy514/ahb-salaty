import '../../../data/models/reset_password.dart';

abstract class ResendCodeStates{}

class ResendCodeLoadingState extends ResendCodeStates{}
class ResendCodeInitState extends ResendCodeStates{}
class ResendCodSelectCountryState extends ResendCodeStates{}

class ResendCodeSuccessState extends ResendCodeStates{
  final MessageModel messageModel;

  ResendCodeSuccessState({required this.messageModel});
}
class ResendCodeErrorState extends ResendCodeStates{
  final String erroeMessage;

  ResendCodeErrorState(this.erroeMessage);
}