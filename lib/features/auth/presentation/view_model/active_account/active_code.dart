import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/features/auth/presentation/view_model/active_account/active_states.dart';

import '../../../data/repo/active_account/active_account.dart';

class ActiveAccountCubit extends Cubit<ActiveAccountStates> {
  ActiveAccountCubit(this.activeAccountRepos) : super(ActiveAccountInitState());

  final ActiveAccountRepos activeAccountRepos;

  static ActiveAccountCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  TextEditingController activateCodeController = TextEditingController();

  TextEditingController activeCodePhoneController = TextEditingController();

  Future activateAccount(
      {required activationCode,
      required phoneNumber,
      required countryCode}) async {
    emit(ActiveAccountLoadingState());
    var data = await activeAccountRepos.activeAccount(
      activationCode: activationCode,
      countryCode: countryCode,
      phoneNumber: phoneNumber,
    );
    data.fold((failure) {
      emit(ActiveAccountErrorState(failure.massege));
    }, (userData) {
      print(userData);
      emit(ActiveAccountSuccessState(userData));
    });
  }
}
