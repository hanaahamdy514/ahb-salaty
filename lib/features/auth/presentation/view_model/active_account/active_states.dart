



import '../../../data/models/father/user.data.dart';

abstract class ActiveAccountStates {}

class ActiveAccountLoadingState extends ActiveAccountStates {}

class ActiveAccountInitState extends ActiveAccountStates {}

class ActiveAccountSuccessState extends ActiveAccountStates {
  final UserModel userData;

  ActiveAccountSuccessState(this.userData);
}

class ActiveAccountErrorState extends ActiveAccountStates {
  final String errorMessage;

  ActiveAccountErrorState(this.errorMessage);
}
