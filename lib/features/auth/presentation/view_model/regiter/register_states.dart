
import '../../../data/models/father/user.data.dart';

abstract class RegisterStats {}

class RegisterInitState extends RegisterStats {}

class RegisterVisibilityState extends RegisterInitState {}

class RegisterLoadingState extends RegisterInitState {}

class RegisterSuccessState extends RegisterInitState {
  final UserModel userModel;

  RegisterSuccessState(this.userModel);
}

class RegisterErrorState extends RegisterInitState {
  final String errorMessage;

  RegisterErrorState(this.errorMessage);
}

class RegisterSelectedCountryCodeState extends RegisterStats {}
