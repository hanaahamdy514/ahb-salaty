import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/features/auth/data/repo/register/register_repo.dart';

import 'package:task1/features/auth/presentation/view_model/regiter/register_states.dart';

class RegisterCubit extends Cubit<RegisterStats> {
  RegisterCubit(this.registerRepo) : super(RegisterInitState());
  final RegisterRepo registerRepo;

  static RegisterCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  bool isVisible = true;
  IconData visibility = Icons.visibility_off;

  void iconVisibility() {
    isVisible = !isVisible;
    visibility = isVisible ? Icons.visibility_off : Icons.visibility;
    emit(RegisterVisibilityState());
  }

  var registeresNumber;
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController reEnterPasswordController = TextEditingController();
  var selectedCountryCode = 20;

  changeSelectedCountryCode(value) {
    selectedCountryCode = value;
    emit(RegisterSelectedCountryCodeState());
  }

  Future userRegister(
      {required phoneNumber,
      required countryCode,
      required password,
      required passwordConfirmation}) async {
    emit(RegisterLoadingState());
    var response = await registerRepo.userRegister(
        phoneNumber: phoneNumber,
        countryCode: countryCode,
        password: password,
        passwordConfirmation: passwordConfirmation);

    response.fold((failure) {
      emit(RegisterErrorState(failure.massege));
    }, (userData) {
      emit(RegisterSuccessState(userData));
    });
  }

}
