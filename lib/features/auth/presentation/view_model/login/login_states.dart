

import '../../../data/models/child_login/child_model.dart';
import '../../../data/models/father/user.data.dart';

abstract class LoginStates {}

class LoginInitState extends LoginStates {}

class LoginVisibilityState extends LoginStates {}

class LoginLoadingState extends LoginStates {}

class ParentLoginSuccessState extends LoginStates {
  final  UserModel userModel;
  ParentLoginSuccessState(this.userModel);
}

class ChildLoginSuccessState extends LoginStates {
  final ChildModel userData;

  ChildLoginSuccessState(this.userData);
}

class LoginErrorState extends LoginStates {
  final String errorMessage;

  LoginErrorState(this.errorMessage);
}

class ChildLoginErrorState extends LoginStates {
  final String errorMessage;

  ChildLoginErrorState(this.errorMessage);
}

class LoginSelectedCountryCodeState extends LoginStates {}
