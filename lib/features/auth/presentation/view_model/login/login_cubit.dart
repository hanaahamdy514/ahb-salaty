import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/features/auth/data/repo/login/login_repo.dart';
import 'package:task1/features/auth/presentation/view_model/login/login_states.dart';
import '../../../data/models/father/user.data.dart';

class LoginCubit extends Cubit<LoginStates> {
  LoginCubit(this.loginRepo) : super(LoginInitState());
  bool isVisible = true;
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final LoginRepo loginRepo;
  IconData visibility = Icons.visibility_off;

  static LoginCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  void iconVisibility() {
    isVisible = !isVisible;
    visibility = isVisible ? Icons.visibility_off : Icons.visibility;
    emit(LoginVisibilityState());
  }

  var selectedCountryCode = 20;

  changeSelectedCountryCode(value) {
    selectedCountryCode = value;
    emit(LoginSelectedCountryCodeState());
  }

  Future parentLogin(
      {countryCode, required phoneNumber, required password}) async {
    emit(LoginLoadingState());
    var data = await loginRepo.parentLogin(
        countryCode: countryCode, phoneNumber: phoneNumber, password: password);
    data.fold((failure) {
      emit(LoginErrorState(failure.massege));
    }, (userData) {
    print(userData.data?.father?.phone);
      emit(ParentLoginSuccessState(userData));
      debugPrint(userData.msg);

    });
  }

  Future childLogin({required userName, required password}) async {
    emit(LoginLoadingState());
    var data =
        await loginRepo.childLogin(userName: userName, password: password);
    data.fold((failure) {
      emit(ChildLoginErrorState(failure.massege));
    }, (userData) {

      emit(ChildLoginSuccessState(userData));
    });
  }
}
