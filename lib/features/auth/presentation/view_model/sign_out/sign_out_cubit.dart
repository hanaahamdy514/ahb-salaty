import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/constants.dart';
import 'package:task1/features/auth/presentation/view_model/sign_out/sign_out_states.dart';

import '../../../data/repo/sign_out/sign_out_repo.dart';

class SignOutCubit extends Cubit<SignOutStates> {
  final SignOutRepo signOutRepo;

  SignOutCubit(this.signOutRepo) : super(SignOutInitState());

  static get(BuildContext context) {
    return BlocProvider.of(context);
  }

  Future signOut() async {
    emit(SignOutLoadingState());
    var response = await signOutRepo.parentSignOut();
    response.fold((failure) {
      emit(SignOutErrorState(failure.massege));
    }, (userData) {

      emit(SignOutSuccessState(userData));
    });
  }
  Future childSignOut() async {
    emit(SignOutLoadingState());
    var response = await signOutRepo.childSignOut();
    response.fold((failure) {
      emit(SignOutErrorState(failure.massege));
    }, (userData) {

      emit(SignOutSuccessState(userData));
    });
  }
}
