import 'package:task1/features/auth/data/models/father/user.data.dart';

abstract class SignOutStates{}
class SignOutInitState extends SignOutStates{}

class SignOutLoadingState extends SignOutStates{}
class SignOutSuccessState extends SignOutStates{
  dynamic userData;

  SignOutSuccessState(this.userData);
}
class SignOutErrorState extends SignOutStates{
  final String errorMessage;

  SignOutErrorState(this.errorMessage);
}