import 'package:flutter/cupertino.dart';

class ProfileModel{
  final String title;
  final Widget screen;

  ProfileModel({required this.title,required this.screen});
}
