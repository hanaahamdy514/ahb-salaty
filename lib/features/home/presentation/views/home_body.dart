import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/core/utils/constants.dart';
import 'package:task1/features/home/presentation/view_model/home_cubit.dart';
import 'package:task1/core/utils/%20widgets/custom_appbar.dart';
import 'package:task1/features/home/presentation/views/profile/profile_list_view.dart';
import 'package:task1/features/home/presentation/views/profile/seting_help_card_in_profile.dart';
import 'package:task1/features/home/presentation/views/profile/sign_out_button.dart';

import '../../../../generated/l10n.dart';

class Profile extends StatelessWidget {
  const Profile({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: screenHeight * .01),
          child: ListView(
            physics: const BouncingScrollPhysics(),
            children: [
              CustomAppbar(
                appBarTitle: S.of(context).profile,
                leading: Icons.notifications_none,
                trailing: Icons.arrow_forward_ios_sharp,
              ),
              SizedBox(
                height: screenHeight * .02,
              ),
              SettingCardInProfile(
                icon: Icons.settings,
                cardLabel: S.of(context).settings,
                iconColor: Colors.amber,
                borderRaduis: const BorderRadius.only(
                    topRight: Radius.circular(40),
                    topLeft: Radius.circular(40)),
              ),
              const SizedBox(
                height: 5,
              ),
              ProfileListView(
                listOfData: BlocProvider.of<HomeCubit>(context)
                    .profileFirstListView(context),
                listLength: BlocProvider.of<HomeCubit>(context)
                    .profileFirstListView(context)
                    .length,
                screen: BlocProvider.of<HomeCubit>(context)
                    .profileFirstListView(context),
              ),
              const SizedBox(
                height: 5,
              ),
              SettingCardInProfile(
                onTap: () {},
                cardLabel: S.of(context).info,
                icon: Icons.info,
                iconColor: AppColors.info,
              ),
              const SizedBox(
                height: 5,
              ),
              ProfileListView(
                borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(30),
                    bottomLeft: Radius.circular(30)),
                listOfData: BlocProvider.of<HomeCubit>(context)
                    .profileSecondListviewItems(context),
                listLength: BlocProvider.of<HomeCubit>(context)
                    .profileSecondListviewItems(context)
                    .length,
                screen: BlocProvider.of<HomeCubit>(context)
                    .profileFirstListView(context),
              ),
              SizedBox(
                height: screenHeight * .01,
              ),
              const SignOutButton(),
              SizedBox(
                height: screenHeight * .02,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
