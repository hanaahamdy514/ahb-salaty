import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/circular_indicator.dart';
import 'package:task1/core/utils/%20widgets/toast.dart';
import 'package:task1/core/utils/constants.dart';
import 'package:task1/core/utils/local_db.dart';
import 'package:task1/features/home/presentation/views/profile/profile_custom_list_tile.dart';
import 'package:task1/features/welcome/presentation/view_model/select_client/selsect_client_cubit.dart';

import '../../../../../core/utils/service_locator.dart';
import '../../../../../generated/l10n.dart';
import '../../../../auth/data/repo/sign_out/sign_out_repo_implementation.dart';
import '../../../../auth/presentation/view_model/sign_out/sign_out_cubit.dart';
import '../../../../auth/presentation/view_model/sign_out/sign_out_states.dart';

class SignOutButton extends StatelessWidget {
  const SignOutButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SignOutCubit(getIt.get<SignOutRepoImplementation>()),
      child:
          BlocConsumer<SignOutCubit, SignOutStates>(listener: (context, state) {
        if (state is SignOutSuccessState) {
          if (state.userData.key == "success") {
            GoRouter.of(context).pushReplacement('/selectClient');
            toast(message: state.userData.msg, color: Colors.green);
          }
          if (state.userData.key == "fail") {
            toast(message: state.userData.msg, color: Colors.red);
          }
        } else if (state is SignOutErrorState) {
          toast(message: state.errorMessage, color: Colors.red);
        }
      }, builder: (context, state) {
        SignOutCubit cubit =
            SignOutCubit(getIt.get<SignOutRepoImplementation>());
        return ConditionalBuilder(
            condition: state is! SignOutLoadingState,
            builder: (context) {
              return Card(
                clipBehavior: Clip.antiAliasWithSaveLayer,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16)),
                child: ProfileCustomListTile(
                    cardHeight: 50,
                    listTileLeading: const Icon(
                      Icons.login,
                      color: Colors.red,
                    ),
                    listTileAction: () async {
                      await LocalHelper.clearData(key: "son_token");
                      await LocalHelper.clearData(key: "father_token");
                      // SelectClientCubit.get(context).clientCategory
                      //     ?
                      cubit.signOut();
                      //: cubit.childSignOut();
                      if(context.mounted){
                        GoRouter.of(context).pushReplacement("/selectClient");
                      }
                    },
                    lablel: S.of(context).logout),
              );
            },
            fallback: (context) => const CircularIndicator());
      }),
    );
  }
}
