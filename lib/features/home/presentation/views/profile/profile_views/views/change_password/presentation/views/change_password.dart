import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/core/utils/service_locator.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/data/repo/change_password_repo_implementation.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/presentation/view_model/change_password_cubit.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/presentation/views/widget/change_passord_body.dart';

class ChangePassword extends StatelessWidget {
  const ChangePassword({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          ChangePasswordCubit(getIt.get<ChangePasswordRepoImp>()),
      child: Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(child: ChangePasswordBody())),
    );
  }
}
