import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/custom_form_field.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/presentation/view_model/change_password_cubit.dart';
import 'package:task1/generated/l10n.dart';

import '../../../../../../../../../../../core/utils/ widgets/custom_appbar.dart';
import '../../../../../../../../../../../core/utils/styles.dart';

ChangePasswordFormFields(
    BuildContext context, ChangePasswordCubit cubit, double screenHeight) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      CustomAppbar(
        appBarTitle: S.of(context).change_password,
        trailingAction: () {
          GoRouter.of(context).pop();
        },
      ),
      const SizedBox(
        height: 20,
      ),
      Text(
        S.of(context).old_password,
        style: Styles.textStyle16,
      ),
      const SizedBox(
        height: 5,
      ),
      CustomFormField(
          suffix: IconButton(
            onPressed: () {
              cubit.iconVisibility();
            },
            icon: Icon(
              cubit.visibility,
              size: 16,
            ),
          ),
          isVisible: cubit.isVisible,
          fillColor: AppColors.darkWhite,
          hint: S.of(context).old_password,
          controller: cubit.oldPassWController),
      const SizedBox(
        height: 5,
      ),
      Text(
        S.of(context).new_password,
        style: Styles.textStyle16,
      ),
      const SizedBox(
        height: 5,
      ),
      CustomFormField(
        isVisible: cubit.isVisible,
        suffix: IconButton(
          onPressed: () {
            cubit.iconVisibility();
          },
          icon: Icon(
            cubit.visibility,
            size: 16,
          ),
        ),
        fillColor: AppColors.darkWhite,
        hint: S.of(context).new_password,
        controller: cubit.newPassWordController,
      ),
      Text(
        S.of(context).confirm_new_password,
        style: Styles.textStyle16,
      ),
      const SizedBox(
        height: 5,
      ),
      CustomFormField(
          isVisible: cubit.isVisible,
          suffix: IconButton(
            onPressed: () {
              cubit.iconVisibility();
            },
            icon: Icon(
              cubit.visibility,
              size: 16,
            ),
          ),
          fillColor: AppColors.darkWhite,
          hint: S.of(context).confirm_new_password,
          controller: cubit.confirmNewPasswordController),
      SizedBox(
        height: screenHeight * .04,
      ),
    ],
  );
}
