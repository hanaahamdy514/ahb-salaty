import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/local_db.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/view_model/laanguage_sttings_cubit.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/views/widget/select_language_custom_button.dart';

import '../../../../../../../../../../../core/utils/app_colors.dart';
import '../../../../../../../../../../../core/utils/images_path.dart';
import '../../view_model/language_setting_states.dart';

class SelectLanguageButtons extends StatelessWidget {
  const SelectLanguageButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChangeLanguageCubit, ChangeLanguageStates>(
        builder: (context, state) {
       ChangeLanguageCubit cubit = ChangeLanguageCubit.get(context);
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SelectLanguageButton(
                      buttonAction: () async {
                         cubit.changeLanguage(lanCode: "en");
                      },
                      image: AppImages.eng,
                      iconTextColor: AppColors.darkBlue,
                      iconLabel: "English"),

                  SelectLanguageButton(
                      buttonAction: () async {

                        BlocProvider.of<ChangeLanguageCubit>(context).changeLanguage(lanCode: "ar");
                      },
                      image: AppImages.ar,
                      iconTextColor: AppColors.green,
                      iconLabel: "اللغه العربيه")
                ],
              );

          }

    );
  }
}
