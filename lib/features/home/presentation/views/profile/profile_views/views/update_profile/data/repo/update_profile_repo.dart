import 'package:dartz/dartz.dart';
import 'package:task1/features/auth/data/models/child_login/child_model.dart';
import 'package:task1/features/auth/data/models/father/user.data.dart';

import '../../../../../../../../../../core/errors/errors.dart';

abstract class UpdateProfileRepo {
  Future<Either<Failure, UserModel>> updateProfile(
      {required name,
      required phone,
      required email,
      required countryCode,
      required String token});

  Future<Either<Failure, ChildModel>> updateSonProfile(
      {required password, required String name, required String token});
}
