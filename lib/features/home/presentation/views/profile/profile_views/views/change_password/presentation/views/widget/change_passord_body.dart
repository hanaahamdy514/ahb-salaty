import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/presentation/view_model/change_password_satates.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/presentation/view_model/change_password_cubit.dart';
import 'change_password_button.dart';
import 'change_password_form_fields.dart';

class ChangePasswordBody extends StatelessWidget {
  ChangePasswordBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChangePasswordCubit, ChangePasswordStates>(
        builder: (context, state) {
      double screenWidth = MediaQuery.of(context).size.width;
      double screenHeight = MediaQuery.of(context).size.height;
      ChangePasswordCubit cubit = ChangePasswordCubit.get(context);
      GlobalKey<FormState>formKey=GlobalKey<FormState>();
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: screenWidth * .04),
        child: Form(key: formKey,
          child: SingleChildScrollView(
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            physics: const BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                ChangePasswordFormFields(context, cubit, screenHeight),
                ChangePasswordButton(formKey:formKey)
              ],
            ),
          ),
        ),
      );
    });
  }


}

