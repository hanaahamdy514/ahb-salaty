import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/custom_appbar.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/presentation/views/widgets/update_son_profile_form_fields.dart';

import '../../../../../../../../../../../generated/l10n.dart';

class UpdateSonProfileBody extends StatelessWidget {
  UpdateSonProfileBody({Key? key}) : super(key: key);
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    return Form(
      key: formKey,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: screenWidth * .04),
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: screenHeight * .01,
              ),
              CustomAppbar(
                appBarTitle: S.of(context).profile,
                trailingAction: () {
                  GoRouter.of(context).pop();
                },
              ),
              UpdateSonProfileFormFields(
                formKey: formKey,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
