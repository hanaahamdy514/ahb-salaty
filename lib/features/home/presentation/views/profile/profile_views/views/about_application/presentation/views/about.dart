import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/custom_appbar.dart';
import 'package:task1/core/utils/styles.dart';
import 'package:task1/generated/l10n.dart';
import '../../../../../../../../../../core/utils/ widgets/cutsom_home_body.dart';

class About extends StatelessWidget {
  const About({super.key});

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
        body: SafeArea(
      child: Column(
        children: [
          CustomAppbar(
              trailingAction: () {
                GoRouter.of(context).pop();
              },
              appBarTitle: S.of(context).about),
          SizedBox(
            height: screenHeight * .15,
          ),
          CustomHomeBody(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  SizedBox(
                    height: screenHeight * .1,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      S.of(context).about_description,
                      textAlign: TextAlign.center,
                      maxLines: 5,
                      style: Styles.textStyle14
                          .copyWith(fontWeight: FontWeight.normal, height: 1.8),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    ));
  }
}
