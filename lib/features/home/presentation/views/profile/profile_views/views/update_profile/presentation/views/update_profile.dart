import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/%20widgets/drop_down_button.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/presentation/views/widgets/update_profile_body.dart';

import '../../../../../../../../../../core/utils/ widgets/custom_form_field.dart';

class UpdateProfile extends StatelessWidget {
  const UpdateProfile({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: SafeArea(child: UpdateProfileBody()),
    );
  }
}
