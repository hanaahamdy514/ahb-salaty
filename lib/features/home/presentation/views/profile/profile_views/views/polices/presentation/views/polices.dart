import 'package:flutter/material.dart';
import 'package:task1/core/utils/%20widgets/custom_appbar.dart';
import 'package:task1/core/utils/%20widgets/cutsom_home_body.dart';

class Polices extends StatelessWidget {
  const Polices({super.key});

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            CustomAppbar(appBarTitle: "سياسة الاستخدام والشروط"),
            SizedBox(
              height: screenHeight * .15,
            ),
            CustomHomeBody(
              child: Column(
                children: [
                  SizedBox(
                    height: screenHeight * .15,
                  ),
                  const Text(
                    "هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد",
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
