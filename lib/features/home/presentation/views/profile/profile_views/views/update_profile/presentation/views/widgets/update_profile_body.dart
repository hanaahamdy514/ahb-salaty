import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/%20widgets/circular_indicator.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/core/utils/styles.dart';
import 'package:task1/features/home/presentation/view_model/home_cubit.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/presentation/views/widgets/update_father_profile_formFields.dart';
import 'package:task1/generated/l10n.dart';

class UpdateProfileBody extends StatelessWidget {
  UpdateProfileBody({Key? key}) : super(key: key);
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return BlocProvider(create: (context)=>HomeCubit()..getUserData()..userModel,
      child: Form(
        key: formKey,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: screenWidth * .04),
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            child: Column(
              children: [
                Text(
                  S.of(context).profile,
                  style: Styles.textStyle20.copyWith(color: AppColors.lightBlack),
                ),
                Visibility(
                    child: BlocProvider.of<HomeCubit>(context).userModel != null
                        ? FatherFormFields()
                        :const CircularIndicator())
              ],
            ),
          ),
        ),
      ),
    );
  }
}
