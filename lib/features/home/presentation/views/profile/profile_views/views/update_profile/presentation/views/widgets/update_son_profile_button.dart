import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/constants.dart';
import 'package:task1/core/utils/local_db.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/presentation/view_model/update_profile_cubit.dart';
import 'package:task1/generated/l10n.dart';

import '../../../../../../../../../../../core/utils/ widgets/default_button.dart';
import '../../view_model/update_profile_states.dart';

class UpdateSonProfileButton extends StatelessWidget {
  const UpdateSonProfileButton({
    Key? key,
    required this.formKey,
  }) : super(key: key);
  final GlobalKey<FormState> formKey;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UpdateProfileCubit, UpdateProfileStates>(
        builder: (context, state) {
      UpdateProfileCubit cubit = UpdateProfileCubit.get(context);
      return ConditionalBuilder(
          condition: state is! UpdateProfileLoadingState,
          builder: (context) {
            return DefaultButton(
                buttonLabel: S.of(context).save_changes,
                onPressed: () async {
                  if (formKey.currentState!.validate()) ;
                  son_token = await LocalHelper.getData(key: "son_token");
                  cubit.updateSonProfile(
                      token: "Bearer $son_token",
                      name: cubit.sonNameController.text,
                      password: cubit.sonPasswordController.text);
                  formKey.currentState!.reset();
                });

          },
          fallback: (context) {
            return const Center(child: CircularProgressIndicator());
          });
    });
  }
}
