abstract class ContactUsStates{}
class ContactUsInitState extends ContactUsStates{}
class SendMessageToContactUsLoadingState extends ContactUsStates{}
class SendMessageToContactUsSuccessState extends ContactUsStates{}
class SendMessageToContactUsErrorState extends ContactUsStates{}
class SendMessageToContactUsSelectedCountryState extends ContactUsStates{}


