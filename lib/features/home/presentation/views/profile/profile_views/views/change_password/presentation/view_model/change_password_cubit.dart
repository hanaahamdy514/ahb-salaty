import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/data/repo/change_password_repo.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/presentation/view_model/change_password_satates.dart';

class ChangePasswordCubit extends Cubit<ChangePasswordStates> {
  ChangePasswordCubit(this.changePasswordRepo)
      : super(ChangePasswordInitState());

  ChangePasswordRepo changePasswordRepo;

  static ChangePasswordCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  bool isVisible = true;

  IconData visibility = Icons.visibility_off;

  void iconVisibility() {
    isVisible = !isVisible;
    visibility = isVisible ? Icons.visibility_off : Icons.visibility;
    emit(ChangePasswordVisibilityState());
  }

  TextEditingController oldPassWController = TextEditingController();
  TextEditingController newPassWordController = TextEditingController();
  TextEditingController confirmNewPasswordController = TextEditingController();

  Future changePassword(
      {required oldPassword,
      required newPassword,
       required String? token,
      required confirmationPassword}) async {
    emit(ChangePasswordLoadingState());

    var response = await changePasswordRepo.changePassword(
      token:token ,
        oldPassword: oldPassword,
        newPassword: newPassword,
        confirmationPassword: confirmationPassword);
    response.fold((failure) {
      emit(ChangePasswordErrorState(failure.massege));
    }, (responseData) {
      print(responseData.message);
      emit(ChangePasswordSuccessState(responseData));
    });
  }
}
