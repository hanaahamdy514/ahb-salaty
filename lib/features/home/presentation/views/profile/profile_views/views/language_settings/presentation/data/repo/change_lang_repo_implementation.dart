import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:task1/core/errors/errors.dart';
import 'package:task1/core/utils/api_constants.dart';
import 'package:task1/features/auth/data/models/reset_password.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/data/repo/change_language_repo.dart';

import '../../../../../../../../../../../core/utils/api_service.dart';

class ChangeLanguageRepoImp implements ChangeLanguageRepo {
  final ApiService apiService;

  ChangeLanguageRepoImp(this.apiService);

  @override
  Future<Either<Failure, MessageModel>> changeLanguage(
      {required String language}) async {
    try {
      var data = await apiService.patch(
          endPoint: ApiConstants.changeLanguage, language: language);

      return right((MessageModel.FomJson(data)));
    } catch (e) {
      if (e is DioException) {
        return left(ServerError.fromDioException(e));
      }
      return left(ServerError(e.toString()));
    }
  }
}
