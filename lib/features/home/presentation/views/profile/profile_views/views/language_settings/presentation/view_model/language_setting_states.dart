import 'package:flutter/material.dart';
import 'package:task1/features/auth/data/models/reset_password.dart';

abstract class ChangeLanguageStates {}

class LanguageSettingsInitState extends ChangeLanguageStates {}
class ChangeLanguageLoadingState extends ChangeLanguageStates {}
class ChangeLanguageSuccessState extends ChangeLanguageStates {
  final MessageModel messageModel;

  ChangeLanguageSuccessState(this.messageModel);
}
class ChangeLanguageErrorState extends ChangeLanguageStates {
  final String errorMessage;

  ChangeLanguageErrorState(this.errorMessage);
}
class ChangeLanguageLocalState extends ChangeLanguageStates {
  final Locale locale;

  ChangeLanguageLocalState(this.locale);
}

