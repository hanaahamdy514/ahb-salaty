import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/%20widgets/default_button.dart';
import 'package:task1/core/utils/constants.dart';
import 'package:task1/core/utils/local_db.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/presentation/view_model/update_profile_cubit.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/presentation/view_model/update_profile_states.dart';
import 'package:task1/generated/l10n.dart';

class UpdateProfileButton extends StatelessWidget {
  const UpdateProfileButton(
      {Key? key,
      required this.state,
      required this.cubit,
      required this.formKey,
      required this.nameController,
      required this.phoneController,
      required this.emailController})
      : super(key: key);
  final UpdateProfileStates state;
  final UpdateProfileCubit cubit;
  final GlobalKey<FormState> formKey;
  final TextEditingController nameController;
  final TextEditingController phoneController;
  final TextEditingController emailController;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UpdateProfileCubit, UpdateProfileStates>(
        builder: (context, state) {
      return ConditionalBuilder(
          condition: state is! UpdateProfileLoadingState,
          builder: (context) {
            return DefaultButton(
                buttonLabel: S.of(context).save_changes,
                onPressed: () async {
                  father_token = await LocalHelper.getData(key: "father_token");
                  print("father token in update profile$father_token");
                  cubit.updateProfile(
                      token: "Bearer $father_token",
                      countryCode: cubit.selectedCountryCode,
                      phone: phoneController.text,
                      name: nameController.text,
                      email: emailController.text);
                });
          },
          fallback: (context) {
            return const Center(child: CircularProgressIndicator());
          });
    });
  }
}
