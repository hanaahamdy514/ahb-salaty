import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/contact_us/presentation/view_model/contact_us_states.dart';

class ContactUsCubit extends Cubit<ContactUsStates> {
  ContactUsCubit() : super(ContactUsInitState());
TextEditingController nameController=TextEditingController();
  TextEditingController phoneController=TextEditingController();
  TextEditingController messageController=TextEditingController();
  var selectedCountryCode = 20;

  changeSelectedCountryCode(value) {
    selectedCountryCode = value;
    emit(SendMessageToContactUsSelectedCountryState());
  }
}
