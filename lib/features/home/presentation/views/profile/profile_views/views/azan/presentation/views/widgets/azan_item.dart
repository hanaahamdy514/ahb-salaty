import 'package:flutter/material.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/core/utils/styles.dart';

class AzanItem extends StatelessWidget {
  const AzanItem({Key? key, required this.title, required this.subTitle, this.onTap}) : super(key: key);
final String title;
final String subTitle;
final void Function()? onTap;
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        onTap:onTap ,
        trailing: const Icon(
          Icons.play_circle,
          size: 30,
          color: AppColors.primaryColor,
        ),
        title:  Text(
          title,
          textAlign: TextAlign.end,
          style: Styles.textStyle14,
        ),
        subtitle: Text(
          subTitle,
          textAlign: TextAlign.end,
          style: Styles.textStyle12.copyWith(color: AppColors.grey),
        ),
      ),
    );
  }
}
