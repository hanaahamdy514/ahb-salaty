import 'package:flutter/material.dart';
import 'package:task1/core/utils/styles.dart';

class SelectLanguageButton extends StatelessWidget {
  const SelectLanguageButton(
      {Key? key,
      required this.image,
      required this.iconTextColor,
      this.buttonAction, required this.iconLabel})

      : super(key: key);
  final String image;
  final String iconLabel;
  final Color iconTextColor;
  final void Function()? buttonAction;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: buttonAction,
      child: Column(
        children: [
          Image(
            width: 90,
            image: AssetImage(
              image,
            ),
          ),
          Text(
            iconLabel,
            style: Styles.langTextStyle16.copyWith(color: iconTextColor),
          )
        ],
      ),
    );
  }
}
