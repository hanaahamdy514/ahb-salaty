// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:task1/core/utils/%20widgets/custom_form_field.dart';
// import 'package:task1/core/utils/%20widgets/cutsom_home_body.dart';
// import 'package:task1/core/utils/%20widgets/drop_down_button.dart';
// import 'package:task1/core/utils/app_colors.dart';
// import 'package:task1/core/utils/styles.dart';
// import 'package:task1/features/home/presentation/views/profile/profile_views/views/contact_us/presentation/view_model/contact_us_cubit.dart';
// import 'package:task1/features/home/presentation/views/profile/profile_views/views/contact_us/presentation/view_model/contact_us_states.dart';
// import 'package:task1/generated/l10n.dart';
//
// class ContactUs extends StatelessWidget {
//   ContactUs({super.key});
//
//   TextEditingController nameController = TextEditingController();
//   TextEditingController phoneController = TextEditingController();
//   TextEditingController messageController = TextEditingController();
//
//   @override
//   Widget build(BuildContext context) {
//     double screenHeight = MediaQuery.of(context).size.height;
//     return Scaffold(
//
//         body: Column(crossAxisAlignment: CrossAxisAlignment.end,
//           children: [
//             SizedBox(
//               height: screenHeight * .25,
//             ),
//             CustomHomeBody(
//                 child: Column(
//               children: [
//                 Padding(
//                   padding: EdgeInsets.symmetric(horizontal: screenHeight * .04),
//                   child: Column(
//                     children: [
//                       SizedBox(
//                         height: screenHeight * .1,
//                       ),
//                       Text(
//                         S.of(context).name,
//                         style: Styles.textStyle13,
//                         textAlign: TextAlign.end,
//
//                       ),
//                       const SizedBox(
//                         height: 5,
//                       ),
//                       CustomFormField(
//                           fillColor: AppColors.darkWhite,
//                           hint: '',
//                           controller: nameController),
//                       const SizedBox(
//                         height: 5,
//                       ),
//                       Text(
//                         S.of(context).phone,
//                         style: Styles.textStyle13,
//                         textAlign: TextAlign.start,
//                       ),
//                       const SizedBox(
//                         height: 5,
//                       ),
//                       CustomFormField(
//                         fillColor: AppColors.darkWhite,
//                         hint: '',
//                         controller: phoneController,
//                         // suffix: dropDownItem(profileCubit),
//                       ),
//                       Text(
//                         S.of(context).emial,
//                         style: Styles.textStyle13,
//                       ),
//                       const SizedBox(
//                         height: 5,
//                       ),
//                     ],
//                   ),
//                 ),
//                 // CustomFormField(
//                 //     fillColor: AppColors.darkWhite,
//                 //     hint: '',
//                 //     controller: emailController),
//               ],
//             ))
//           ],
//         ));
//   }
// }

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/custom_appbar.dart';
import 'package:task1/core/utils/%20widgets/custom_form_field.dart';
import 'package:task1/core/utils/%20widgets/cutsom_home_body.dart';
import 'package:task1/core/utils/%20widgets/default_button.dart';
import 'package:task1/core/utils/%20widgets/drop_down_button.dart';

import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/contact_us/presentation/view_model/contact_us_cubit.dart';
import 'package:task1/generated/l10n.dart';

class ContactUs extends StatelessWidget {
  ContactUs({Key? key}) : super(key: key);
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    return BlocProvider(
        create: (context) => ContactUsCubit(),
        child: Scaffold(
          body: SafeArea(
              child: Column(
            children: [
              CustomAppbar(
                appBarTitle: S.of(context).contact_us,
                trailingAction: () {
                  GoRouter.of(context);
                },
              ),
              SizedBox(
                height: screenHeight * .2,
              ),
              CustomHomeBody(

                  child: Padding(
                padding: EdgeInsets.symmetric(horizontal: screenHeight * .03),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: screenHeight * .1,
                    ),
                    Text(S.of(context).name),
                    SizedBox(
                      height: screenHeight * .01,
                    ),
                    CustomFormField(
                        fillColor: Colors.grey[200],
                        hint: S.of(context).enter_name,
                        controller: nameController),
                    SizedBox(
                      height: screenHeight * .01,
                    ),
                    Text(S.of(context).phone),
                    SizedBox(
                      height: screenHeight * .01,
                    ),
                    CustomFormField(
                        suffix: dropDownItem(ContactUsCubit()),
                        fillColor: Colors.grey[200],
                        hint: S.of(context).phone,
                        controller: phoneController),
                    SizedBox(
                      height: screenHeight * .01,
                    ),
                    Text(S.of(context).message_content),
                    SizedBox(
                      height: screenHeight * .01,
                    ),
                    CustomFormField(
                        fillColor: Colors.grey[200],
                        hint: S.of(context).message_content,
                        controller: phoneController),
                    SizedBox(
                      height: screenHeight * .04,
                    ),
                    DefaultButton(
                      buttonLabel: S.of(context).send,
                      onPressed: () {},
                    )
                  ],
                ),
              ))
            ],
          )),
        ));
  }
}
//CustomScrollView(
//         keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
//         slivers: [
//
//           SliverFillRemaining(
//             child: CustomHomeBody(
//               child: Column(
//             children: [
//               CustomFormField(
//                 hint: "",
//                 controller: nameController,
//               )
//             ],
//               ),
//             ),
//           )
//         ],
//       ),
//Column(
//               crossAxisAlignment: CrossAxisAlignment.end,
//               children: [
//                 UpdateProfileFormFields(context, UpdateProfileCubit(getIt.get<UpdateProfileRepoImpl>())),
//                 SizedBox(
//                   height: screenHeight * .04,
//                 ),
//                 ConditionalBuilder(
//                   condition: true,
//                   fallback: (context) => const CircularIndicator(),
//                   builder: (context) => DefaultButton(
//                     buttonLabel: S.of(context).save_changes,
//                     onPressed: () {
//
//                     },
//                   ),
//                 )
//               ],
//             )
