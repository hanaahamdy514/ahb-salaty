import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/custom_appbar.dart';
import 'package:task1/core/utils/%20widgets/custom_form_field.dart';
import 'package:task1/core/utils/%20widgets/toast.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/presentation/views/widgets/update_son_profile_button.dart';
import 'package:task1/generated/l10n.dart';

import '../../view_model/update_profile_cubit.dart';
import '../../view_model/update_profile_states.dart';

class UpdateSonProfileFormFields extends StatelessWidget {
  const UpdateSonProfileFormFields({
    Key? key,
    required this.formKey,
  }) : super(key: key);
  final GlobalKey<FormState> formKey;

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return BlocConsumer<UpdateProfileCubit, UpdateProfileStates>(
      listener: (context, state) {
        if (state is UpdateSonProfileSuccessState) {
          if (state.model.key == "success") {
            toast(message: state.model.msg, color: Colors.green);
            GoRouter.of(context).pop();
          }
          if (state.model.key == "fail") {
            toast(message: state.model.msg, color: Colors.red);
          }
        }
      },
      builder: (context, state) {
        UpdateProfileCubit cubit = UpdateProfileCubit.get(context);
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: screenHeight * .01,
            ),
            Text(S.of(context).name),
            SizedBox(
              height: screenHeight * .01,
            ),
            CustomFormField(
                fillColor: AppColors.grey.withOpacity(.2),
                hint: S.of(context).name,
                controller: cubit.sonNameController),
            SizedBox(
              height: screenHeight * .01,
            ),
            Text(S.of(context).password),
            SizedBox(
              height: screenHeight * .01,
            ),
            CustomFormField(
                fillColor: AppColors.grey.withOpacity(.2),
                hint: S.of(context).password,
                controller: cubit.sonPasswordController),
            SizedBox(
              height: screenHeight * .02,
            ),
            UpdateSonProfileButton(formKey: formKey)
          ],
        );
      },
    );
  }
}
