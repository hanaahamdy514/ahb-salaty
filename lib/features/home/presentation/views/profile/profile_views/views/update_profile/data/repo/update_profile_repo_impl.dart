import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:task1/core/errors/errors.dart';
import 'package:task1/core/utils/api_constants.dart';
import 'package:task1/core/utils/api_service.dart';
import 'package:task1/features/auth/data/models/child_login/child_model.dart';
import 'package:task1/features/auth/data/models/father/user.data.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/data/repo/update_profile_repo.dart';

class UpdateProfileRepoImpl implements UpdateProfileRepo {
  final ApiService apiService;

  UpdateProfileRepoImpl(this.apiService);

  @override
  Future<Either<Failure, UserModel>> updateProfile({required countryCode,
    required name,
    required phone,
    required email,
    required String token}) async {
    try {
      var response = await apiService.postData(
          token: token,
          query: {"_method": "put"},
          endPoint: ApiConstants.updateProfile,
          data: {
            "country_code": countryCode,
            "phone": phone,
            "name": name,
            "email": email
          });

      return right(UserModel.fromJson(response));
    } catch (e) {
      if (e is DioException) {
        return left(ServerError.fromDioException(e));
      }
      return left(ServerError(e.toString()));
    }
  }

  @override
  Future<Either<Failure, ChildModel>> updateSonProfile(
      {required password, required String name, required String token}) async {
    try {
      var response = await apiService.postData(
          token: token,
          query: {"_method": "put"},
          endPoint: ApiConstants.updateSonProfile,
          data: {
            "name": name,
            "password": password
          });

      return right(ChildModel.fromJson(response));
    } catch (e) {
      if (e is DioException) {
        return left(ServerError.fromDioException(e));
      }
      return left(ServerError(e.toString()));
    }
  }}
