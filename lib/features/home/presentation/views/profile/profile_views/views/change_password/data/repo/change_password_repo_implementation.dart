import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:task1/core/errors/errors.dart';
import 'package:task1/core/utils/api_constants.dart';
import 'package:task1/core/utils/api_service.dart';
import 'package:task1/features/auth/data/models/reset_password.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/data/repo/change_password_repo.dart';

class ChangePasswordRepoImp implements ChangePasswordRepo {
  final ApiService apiService;

  ChangePasswordRepoImp(this.apiService);

  @override
  Future<Either<Failure, MessageModel>> changePassword(
      {required oldPassword,
      required newPassword,
      required String? token,
      required confirmationPassword}) async {
    try {
      var response = await apiService.postData(
          token: "Bearer $token",
          query: {"_method": "patch"},
          endPoint: ApiConstants.changePassword,
          data: {
            "password": newPassword,
            "password_confirmation": confirmationPassword,
            "old_password": oldPassword
          });
      return right(MessageModel.FomJson(response));
    } catch (e) {
      if (e is DioException) {
        return left(ServerError.fromDioException(e));
      }
      return left(ServerError(e.toString()));
    }
  }
}
