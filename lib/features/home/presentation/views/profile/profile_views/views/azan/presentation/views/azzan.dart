import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/custom_appbar.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/azan/presentation/views/widgets/azan_list_view.dart';

class Azan extends StatelessWidget {
  const Azan({super.key});

  @override
  Widget build(BuildContext context) {
    double screenWidth=MediaQuery.of(context).size.width;
    return Scaffold(
        body: SafeArea(
      child: Padding(
        padding:   EdgeInsets.symmetric(horizontal:screenWidth*.04),
        child: Column(

          children: [
            CustomAppbar(
              appBarTitle: "تحديد الاذان",
              trailingAction: () {
                GoRouter.of(context).pop();
              },
            ),
           const AzanListView()

          ],
        ),
      ),
    ));
  }
}
