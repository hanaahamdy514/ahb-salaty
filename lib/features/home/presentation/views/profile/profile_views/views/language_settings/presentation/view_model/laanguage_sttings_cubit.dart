import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/lang_cach_helper.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/data/repo/change_language_repo.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/view_model/language_setting_states.dart';

class ChangeLanguageCubit extends Cubit<ChangeLanguageStates> {
  ChangeLanguageCubit(this.changeLanguageRepo)
      : super(LanguageSettingsInitState());
  final ChangeLanguageRepo changeLanguageRepo;

  static ChangeLanguageCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  Future getSavedLanguage() async {
    final String cachedLangCode =
        await LanguageCacheHelper().getCachedLanguageCode();
    emit(ChangeLanguageLocalState(Locale(cachedLangCode)));
  }

  Future changeLanguage({required String lanCode}) async {
    await LanguageCacheHelper().cacheLanguageCode(lanCode);
    emit(ChangeLanguageLocalState(Locale(lanCode)));
  }
  //
  // Future changeLanguage({required lang}) async {
  //   emit(ChangeLanguageLoadingState());
  //   var data = await changeLanguageRepo.changeLanguage(language: lang);
  //   data.fold((failure) {
  //     emit(ChangeLanguageErrorState(failure.massege));
  //   }, (userData) {
  //     debugPrint(userData.message);
  //     debugPrint(userData.key);
  //     emit(ChangeLanguageSuccessState(userData));
  //   });
  // }
}
