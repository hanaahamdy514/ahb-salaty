import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/default_button.dart';
import 'package:task1/core/utils/%20widgets/toast.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/core/utils/constants.dart';
import 'package:task1/core/utils/local_db.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/presentation/view_model/change_password_cubit.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/presentation/view_model/change_password_satates.dart';
import 'package:task1/features/welcome/presentation/view_model/select_client/selsect_client_cubit.dart';
import 'package:task1/generated/l10n.dart';

import '../../../../../../../../../../../core/utils/ widgets/circular_indicator.dart';

class ChangePasswordButton extends StatelessWidget {
  const ChangePasswordButton({Key? key, required this.formKey})
      : super(key: key);
  final GlobalKey formKey;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ChangePasswordCubit, ChangePasswordStates>(
      listener: (context, state) {
        if (state is ChangePasswordSuccessState) {
          if (state.messageModel.key == "success") {
            toast(message: state.messageModel.message, color: AppColors.green);
            GoRouter.of(context).pop();
          } else {
            toast(message: state.messageModel.message, color: Colors.red);
          }
        } else if (state is ChangePasswordErrorState) {
          toast(message: state.errorMessage, color: Colors.red);
        }
      },
      builder: (context, state) {
        ChangePasswordCubit cubit = ChangePasswordCubit.get(context);
        return ConditionalBuilder(
          condition: state is! ChangePasswordLoadingState,
          builder: (context) {
            return DefaultButton(
              buttonLabel: S.of(context).save_changes,
              onPressed: () async {
                father_token = await LocalHelper.getData(key: "father_token");
                cubit.changePassword(
                    token: father_token,
                    oldPassword: cubit.oldPassWController.text,
                    newPassword: cubit.newPassWordController.text,
                    confirmationPassword:
                        cubit.confirmNewPasswordController.text);
              },
            );
          },
          fallback: (context) => const CircularIndicator(),
        );
      },
    );
  }
}
