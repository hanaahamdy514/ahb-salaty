import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:task1/core/utils/%20widgets/custom_appbar.dart';
import 'package:task1/core/utils/%20widgets/cutsom_home_body.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/core/utils/images_path.dart';
import 'package:task1/core/utils/styles.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/social_media/presentation/widgets/social_icon.dart';
import 'package:task1/generated/l10n.dart';

class SocialMedia extends StatelessWidget {
  const SocialMedia({super.key});

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            CustomAppbar(appBarTitle: S.of(context).social_media),
            SizedBox(
              height: screenHeight * .15,
            ),
            CustomHomeBody(
                child: Column(
              children: [
                SizedBox(
                  height: screenHeight * .1,
                ),
                Text(
                  S.of(context).social_media,
                  style:
                      Styles.textStyle20.copyWith(color: AppColors.lightBlack),
                ),
                SizedBox(
                  height: screenHeight * .05,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SocialIcon(
                      icon: const Icon(
                        FontAwesomeIcons.youtube,
                        color: Colors.white,
                      ),
                      backgroundColor: Colors.red,
                    ),
                    SocialIcon(
                      icon: const Icon(
                        FontAwesomeIcons.snapchat,
                      ),
                      backgroundColor: Colors.yellowAccent,
                    ),
                    SocialIcon(backgroundColor: Colors.white,
                      icon:Icon( FontAwesomeIcons.instagram,)
                    ),
                    SocialIcon(
                        icon: const Icon(
                      FontAwesomeIcons.twitter,
                      color: AppColors.white,
                    ))
                  ],
                )
              ],
            ))
          ],
        ),
      ),
    );
  }
}
