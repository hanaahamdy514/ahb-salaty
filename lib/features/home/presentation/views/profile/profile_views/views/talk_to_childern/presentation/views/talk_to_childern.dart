import 'package:flutter/material.dart';

class TalkToChildren extends StatelessWidget {
  const TalkToChildren({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
     body: Center(
        child: Text("Talk to children"),
      ),
    );
  }
}
