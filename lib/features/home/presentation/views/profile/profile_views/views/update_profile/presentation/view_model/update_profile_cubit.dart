import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/data/repo/update_profile_repo.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/presentation/view_model/update_profile_states.dart';

class UpdateProfileCubit extends Cubit<UpdateProfileStates> {
  UpdateProfileCubit(this.updateProfileRepo) : super(UpdateProfileInitState());

  static UpdateProfileCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }


  TextEditingController sonNameController = TextEditingController();
  TextEditingController sonPasswordController = TextEditingController();

  final UpdateProfileRepo updateProfileRepo;
  var selectedCountryCode = 20;

  changeSelectedCountryCode(value) {
    selectedCountryCode = value;
    emit(UpdateProfileSelectedCountryState());
  }

  Future updateProfile(
      {required countryCode,
      required phone,
      required name,
      required String token,
      required email}) async {
    emit(UpdateProfileLoadingState());
    var response = await updateProfileRepo.updateProfile(
        token: token,
        name: name,
        phone: phone,
        email: email,
        countryCode: countryCode);
    response.fold((failure) {
      emit(UpdateProfileErrorState(failure.massege));
    }, (responseData) {
      emit(UpdateProfileSuccessState(responseData));
    });
  }

  Future updateSonProfile(
      {required name, required String? token, required password}) async {
    emit(UpdateProfileLoadingState());
    var response = await updateProfileRepo.updateSonProfile(
      token: token ?? "",
      name: name,
      password: password,
    );
    response.fold((failure) {
      emit(UpdateSonProfileErrorState(failure.massege));
    }, (responseData) {
      print(responseData.msg);
      emit(UpdateSonProfileSuccessState(responseData));
    });
  }
}
