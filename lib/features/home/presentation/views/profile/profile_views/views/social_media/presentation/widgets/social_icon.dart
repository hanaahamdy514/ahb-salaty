import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SocialIcon extends StatelessWidget {
  SocialIcon({
    Key? key,
    this.onPressed,
    required this.icon,  this.backgroundColor,
  }) : super(key: key);
  final Widget icon;
  final void Function()? onPressed;
  final Color? backgroundColor;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 60,
        width: 50,
        child: CircleAvatar(
            backgroundColor: backgroundColor,
            child: IconButton(onPressed: () {}, icon: icon)));
  }
}
