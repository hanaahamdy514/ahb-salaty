import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/drop_down_button.dart';
import 'package:task1/core/utils/%20widgets/toast.dart';
import 'package:task1/features/home/presentation/view_model/home_cubit.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/presentation/views/widgets/update_profile_button.dart';

import '../../../../../../../../../../../core/utils/ widgets/custom_form_field.dart';
import '../../../../../../../../../../../core/utils/app_colors.dart';
import '../../../../../../../../../../../generated/l10n.dart';
import '../../view_model/update_profile_cubit.dart';
import '../../view_model/update_profile_states.dart';

class FatherFormFields extends StatelessWidget {
  FatherFormFields({Key? key}) : super(key: key);
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    HomeCubit userModel = BlocProvider.of<HomeCubit>(context)..userModel;
     nameController.text =userModel .userModel?.data?.father?.name??"";
     phoneController.text = userModel.userModel?.data?.father?.phone??"";
     emailController.text = userModel.userModel?.data?.father?.email??"";

    double screenHeight = MediaQuery.of(context).size.height;
    return BlocConsumer<UpdateProfileCubit, UpdateProfileStates>(
      listener: (context, state) {
        if (state is UpdateProfileSuccessState) {
          if (state.messageModel.key == "success") {
            toast(message: state.messageModel.msg, color: Colors.green);
            GoRouter.of(context).pop();
          } else if (state.messageModel.key == "fail") {
            toast(message: state.messageModel.msg, color: Colors.green);
          }
        } else if (state is UpdateProfileErrorState) {
          toast(
            color: Colors.red,
            message: state.errorMessage,
          );
        }
      },
      builder: (context, state) {
        UpdateProfileCubit cubit = UpdateProfileCubit.get(context);
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: screenHeight * .01,
            ),
            Text(
              S.of(context).name,
              textAlign: TextAlign.start,
            ),
            SizedBox(
              height: screenHeight * .01,
            ),
            CustomFormField(
              hint: S.of(context).name,
              controller: nameController,
              fillColor: AppColors.grey.withOpacity(.2),
            ),
            SizedBox(
              height: screenHeight * .01,
            ),
            Text(S.of(context).phone),
            SizedBox(
              height: screenHeight * .01,
            ),
            CustomFormField(
              suffix: dropDownItem(cubit),
              hint: S.of(context).phone,
              fillColor: AppColors.grey.withOpacity(.2),
              controller: phoneController,
            ),
            SizedBox(
              height: screenHeight * .01,
            ),
            Text(S.of(context).emial),
            SizedBox(
              height: screenHeight * .01,
            ),
            CustomFormField(
                fillColor: AppColors.grey.withOpacity(.2),
                hint: S.of(context).emial,
                controller: emailController),
            SizedBox(
              height: screenHeight * .02,
            ),
            UpdateProfileButton(
              state: state,
              cubit: cubit,
              formKey: formKey,
              nameController: nameController,
              phoneController: phoneController,
              emailController: emailController,
            )
          ],
        );
      },
    );
  }
}
