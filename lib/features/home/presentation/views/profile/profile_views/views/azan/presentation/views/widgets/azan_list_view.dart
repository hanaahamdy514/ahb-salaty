import 'package:flutter/material.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/azan/presentation/views/widgets/azan_item.dart';

class AzanListView extends StatelessWidget {
  const AzanListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) => const AzanItem(
                title: 'مشاري بن راشد العفاسي',
                subTitle: 'مقام العجم',
              ),
          itemCount: 5),
    );
  }
}
