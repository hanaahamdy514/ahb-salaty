import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:task1/core/utils/%20widgets/custom_appbar.dart';
import 'package:task1/core/utils/service_locator.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/data/repo/change_lang_repo_implementation.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/data/repo/change_language_repo.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/view_model/laanguage_sttings_cubit.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/views/widget/select_language_body.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/views/widget/select_language_buttons.dart';
import 'package:task1/generated/l10n.dart';

class LanguageSettings extends StatelessWidget {
  const LanguageSettings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              CustomAppbar(
                trailingAction: (){
                  GoRouter.of(context).pop();
                },
                appBarTitle: S.of(context).app_lang,
              ),
              SizedBox(
                height: screenHeight * .17,
              ),
             const SelectLanguageBody()
            ],
          ),
        ),

    );
  }
}
