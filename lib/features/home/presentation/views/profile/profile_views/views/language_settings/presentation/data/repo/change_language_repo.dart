import 'package:dartz/dartz.dart';
import 'package:task1/core/errors/errors.dart';
import 'package:task1/features/auth/data/models/reset_password.dart';

abstract class ChangeLanguageRepo {
  Future<Either<Failure, MessageModel>> changeLanguage(
      {required String language});
}
