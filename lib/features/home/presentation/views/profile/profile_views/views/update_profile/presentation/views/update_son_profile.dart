import 'package:flutter/material.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/presentation/views/widgets/update_son_profile_body.dart';


class UpdateSonProfile extends StatelessWidget {
  UpdateSonProfile({Key? key}) : super(key: key);
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: SafeArea(child: UpdateSonProfileBody()));
  }
}
