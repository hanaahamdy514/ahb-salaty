import 'package:task1/features/auth/data/models/father/user.data.dart';
import 'package:task1/features/auth/data/models/reset_password.dart';

import '../../../../../../../../../auth/data/models/child_login/child_model.dart';

abstract class UpdateProfileStates {}

class UpdateProfileInitState extends UpdateProfileStates {}

class UpdateProfileSelectedCountryState extends UpdateProfileStates {}

class UpdateProfileLoadingState extends UpdateProfileStates {}

class UpdateProfileSuccessState extends UpdateProfileStates {
  final UserModel messageModel;

  UpdateProfileSuccessState(this.messageModel);
}

class UpdateProfileErrorState extends UpdateProfileStates {
  final String errorMessage;

  UpdateProfileErrorState(this.errorMessage);
}

class UpdateSonProfileLoadingState extends UpdateProfileStates {}

class UpdateSonProfileSuccessState extends UpdateProfileStates {
 final ChildModel model;

  UpdateSonProfileSuccessState(this.model);
}

class UpdateSonProfileErrorState extends UpdateProfileStates {

  final String errorMsg;

  UpdateSonProfileErrorState(this.errorMsg);
}
