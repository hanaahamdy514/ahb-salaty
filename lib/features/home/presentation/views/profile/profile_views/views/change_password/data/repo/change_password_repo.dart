import 'package:dartz/dartz.dart';
import 'package:task1/core/errors/errors.dart';
import 'package:task1/features/auth/data/models/reset_password.dart';

abstract class ChangePasswordRepo {
  Future<Either<Failure, MessageModel>> changePassword(
      {required oldPassword, required newPassword, required confirmationPassword,required String? token});
}
