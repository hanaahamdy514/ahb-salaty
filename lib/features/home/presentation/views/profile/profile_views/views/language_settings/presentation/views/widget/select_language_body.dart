import 'package:flutter/material.dart';

import 'package:task1/core/utils/styles.dart';
import 'package:task1/generated/l10n.dart';


import '../../../../../../../../../../../core/utils/ widgets/cutsom_home_body.dart';
import 'select_language_buttons.dart';

class SelectLanguageBody extends StatelessWidget {
  const SelectLanguageBody({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    return CustomHomeBody(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
      children: [
          SizedBox(
            height: screenHeight * .2,
          ),
          // Text(
          // // LocaleKeys.applang.tr(),
          //  // S.of(context).app_lang,
          //   style:const TextStyle(fontSize: 22, fontWeight: FontWeight.w500),
          // ),
          SizedBox(
            height: screenHeight * .02,
          ),
           Text(
           S.of(context).app_lang_hint,
            style: Styles.textStyle18,
             textAlign: TextAlign.center,
          ),
          SizedBox(
            height: screenHeight * .06,
          ),
          const SelectLanguageButtons()
      ],
    ),
        ));
  }
}
