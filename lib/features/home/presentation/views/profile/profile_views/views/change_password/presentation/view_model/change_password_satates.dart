import 'package:task1/features/auth/data/models/reset_password.dart';

abstract class ChangePasswordStates {}

class ChangePasswordInitState extends ChangePasswordStates {}

class ChangePasswordLoadingState extends ChangePasswordStates {}
class ChangePasswordVisibilityState extends ChangePasswordStates {}
class ChangePasswordSuccessState extends ChangePasswordStates {
  final MessageModel messageModel;

  ChangePasswordSuccessState(this.messageModel);
}
class ChangePasswordErrorState extends ChangePasswordStates {
 final String errorMessage;

  ChangePasswordErrorState(this.errorMessage);
}



