import 'package:flutter/material.dart';

import '../../../../../core/utils/app_colors.dart';

class SettingCardInProfile extends StatelessWidget {
  const SettingCardInProfile(
      {Key? key,
      this.cardLabel,
      required this.icon,
      required this.iconColor,
      this.borderRaduis,
      this.onTap})
      : super(key: key);
  final String? cardLabel;
  final IconData icon;
  final Color iconColor;
  final BorderRadius? borderRaduis;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5),
      decoration:  BoxDecoration(
          color: Colors.white,
          borderRadius: borderRaduis),
      child: ListTile(

        onTap: onTap,
        title: Text(
          cardLabel ?? "",

        ),
        trailing: Icon(icon, color: iconColor),
      ),
    );
  }
}
