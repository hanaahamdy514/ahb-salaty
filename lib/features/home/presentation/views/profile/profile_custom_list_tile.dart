import 'package:flutter/material.dart';
import 'package:task1/core/utils/styles.dart';

import '../../../../../core/utils/app_colors.dart';

class ProfileCustomListTile extends StatelessWidget {
  const ProfileCustomListTile(
      {Key? key,
      required this.lablel,
      this.listTileAction,
      this.listTileLeading,
      this.cardHeight})
      : super(key: key);
  final Icon? listTileLeading;
  final String lablel;
  final void Function()? listTileAction;
  final double? cardHeight;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: cardHeight ?? 40,
      child: Card(
        color: Colors.white,
        shape: const RoundedRectangleBorder(borderRadius: BorderRadius.zero),
        margin: EdgeInsets.zero,
        elevation: 0,
        child: ListTile(
          onTap: listTileAction,
          trailing: listTileLeading ??
              const Icon(
                Icons.arrow_forward_ios_sharp,
                size: 12,
              ),
          title: Text(
            lablel,
            style: Styles.textStyle14,
          ),
        ),
      ),
    );
  }
}
