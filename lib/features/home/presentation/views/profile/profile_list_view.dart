import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/features/home/presentation/view_model/home_cubit.dart';
import 'package:task1/features/home/presentation/views/profile/profile_custom_list_tile.dart';

import '../../../data/models/profile_model.dart';

class ProfileListView extends StatelessWidget {
  const ProfileListView(
      {Key? key,
      required this.listOfData,
      required this.listLength,
      required this.screen,
       this.borderRadius})
      : super(key: key);
  final List listOfData;
  final int listLength;
  final screen;
  final BorderRadius? borderRadius;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 5),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      decoration: BoxDecoration(
borderRadius: borderRadius      ),

      child: SizedBox(
        child: ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: listLength,
            separatorBuilder: (context, index) {
              return const SizedBox(
                height: 3,
              );
            },
            itemBuilder: (context, index) {
              return ProfileCustomListTile(
                listTileAction: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => listOfData[index].screen));
                },
                lablel: listOfData[index].title,
              );
            }),
      ),
    );
  }
}
