
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/api_constants.dart';
import 'package:task1/core/utils/api_service.dart';
import 'package:task1/core/utils/constants.dart';
import 'package:task1/core/utils/local_db.dart';
import 'package:task1/features/auth/data/models/child_login/child_model.dart';
import 'package:task1/features/auth/data/models/father/user.data.dart';
import 'package:task1/features/home/presentation/views/home_body.dart';
import 'package:task1/features/home/presentation/view_model/home_states.dart';
import 'package:task1/features/home/presentation/views/notification.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/about_application/presentation/views/about.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/azan/presentation/views/azzan.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/presentation/views/change_password.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/change_password/presentation/views/change_son_password.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/common_question/presentation/views/common_question.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/contact_us/presentation/views/contact_us.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/views/language_settings.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/notification/presentation/views/notification.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/polices/presentation/views/polices.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/saved_url/presentation/views/saved_url.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/social_media/presentation/views/social_media.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/talk_to_childern/presentation/views/talk_to_childern.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/presentation/views/update_profile.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/presentation/views/update_son_profile.dart';
import 'package:task1/features/welcome/presentation/view_model/select_client/selsect_client_cubit.dart';
import '../../../../generated/l10n.dart';
import '../../data/models/profile_model.dart';

class HomeCubit extends Cubit<HomeStates> {
  HomeCubit() : super(HomeInitState());

  static HomeCubit get(BuildContext context) {
    return BlocProvider.of(context);
  }

  int bottomCurrentIndex = 0;

  void updateSelectedIndex(int index) {
    bottomCurrentIndex = index;
    emit(BottomNavigationIndexState());
  }

  List<Widget> screens = [
    const Profile(),
    NotificationView(),
  ];
  var bottomNavItems = [
    const BottomNavigationBarItem(
        icon: Icon(Icons.perm_identity), label: "حسابي"),
    const BottomNavigationBarItem(
        icon: Icon(Icons.leaderboard_outlined), label: "احصائياتي"),
  ];

  List profileFirstListView(BuildContext context) {
    return [
      ProfileModel(
          title: S.of(context).update_profile,
          screen: BlocProvider.of<SelectClientCubit>(context).clientCategory
              ? const UpdateProfile()
              : UpdateSonProfile()),

      BlocProvider.of<SelectClientCubit>(context).clientCategory?
      ProfileModel(
          title: S.of(context).change_password, screen: const ChangePassword()
      ):  ProfileModel(
          title: "", screen: const Divider()
      ),
      ProfileModel(
          title: S.of(context).notifications, screen: const Notifications()),
      ProfileModel(title: S.of(context).saved_url, screen: const SavedUrl()),
      ProfileModel(
          title: S.of(context).language_settings,
          screen: const LanguageSettings()),
      ProfileModel(title: S.of(context).azzan, screen: const Azan()),
      ProfileModel(
          title: S.of(context).talk_to_parent, screen: const TalkToChildren()),
    ];
  }

  List<ProfileModel> profileSecondListviewItems(BuildContext context) {
    return [
      ProfileModel(title: S.of(context).about, screen: const About()),
      ProfileModel(title: S.of(context).polices, screen: const Polices()),
      ProfileModel(
          title: S.of(context).common_question,
          screen: const CommonQuestions()),
      ProfileModel(title: S.of(context).contact_us, screen: ContactUs()),
      ProfileModel(
          title: S.of(context).social_media, screen: const SocialMedia()),
    ];
  }

   UserModel?userModel;
  ChildModel? childModel;

  void getUserData() async {
    father_token=await LocalHelper.getData(key: "father_token");
    emit(GetUserDataLoadingState());
    final response = await ApiService().get(token: father_token, endPoint: ApiConstants.getFatherProfile);
    userModel = UserModel.fromJson(response);
    print("phone in get method${userModel?.data?.father?.phone}");
    print(userModel?.data?.father?.email);
    emit(GetUserDataSuccessState());
  }


// tokens() async {
//   son_token = await LocalHelper.getData(key: "son_token");
//   father_token = await LocalHelper.getData(key: "father_token");
//   print("$son_token son in home cubit");
//   print("$father_token parent in home cubit");
//   emit(GetTokenState());
// }
}
