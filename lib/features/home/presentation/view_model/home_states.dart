import 'package:task1/core/utils/local_db.dart';

abstract class HomeStates {}

class HomeInitState extends HomeStates {

}
class GetTokenState extends HomeStates {}

class BottomNavigationIndexState extends HomeStates {}
class GetUserDataLoadingState extends HomeStates {}
class GetUserDataSuccessState extends HomeStates {}
class GetUserDataErrorState extends HomeStates {}
