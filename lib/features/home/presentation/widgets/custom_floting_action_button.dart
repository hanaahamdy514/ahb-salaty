import 'package:flutter/material.dart';
import 'package:task1/core/utils/app_colors.dart';

import '../../../../core/utils/images_path.dart';

class CustomFloatingActionButton extends StatelessWidget {
  const CustomFloatingActionButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 80,
        width: 80,
        child: FloatingActionButton(
          elevation: 0,
          backgroundColor: AppColors.darkWhite,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(

            borderRadius: BorderRadius.circular(100),
          ),
          onPressed: () {},
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Center(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(40),
                  child: Image.asset(
                    AppImages.logo,
                    height: 60,
                    width: 60,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
