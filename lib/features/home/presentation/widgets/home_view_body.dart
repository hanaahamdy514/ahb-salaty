import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task1/core/utils/app_colors.dart';
import 'package:task1/features/home/presentation/view_model/home_cubit.dart';
import 'package:task1/features/home/presentation/view_model/home_states.dart';
import 'package:task1/features/home/presentation/widgets/custom_floting_action_button.dart';

class HomeViewBody extends StatelessWidget {
  const HomeViewBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
   return BlocBuilder<HomeCubit, HomeStates>(builder: (context, state) {
        HomeCubit cubit = HomeCubit.get(context);
        return Scaffold(
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          floatingActionButton: const CustomFloatingActionButton(),
          bottomNavigationBar: BottomNavigationBar(
              onTap: (index) {
                cubit.updateSelectedIndex(index);
              },
              selectedItemColor: AppColors.primaryColor,
              currentIndex: cubit.bottomCurrentIndex,
              items: cubit.bottomNavItems),
          body: cubit.screens[cubit.bottomCurrentIndex],
        );
      });

  }
}
