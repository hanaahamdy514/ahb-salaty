// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Tell your children and future generations`
  String get tell_your_children {
    return Intl.message(
      'Tell your children and future generations',
      name: 'tell_your_children',
      desc: '',
      args: [],
    );
  }

  /// `That their affairs cannot be accomplished except by maintaining prayer, and that prayer is a safety and a protection against evil and the gateway to sustenance, and that negligence in it is great in the sight of God, and that he who does not pray will not succeed and none of the affairs of this world or the hereafter will be fulfilled for him.`
  String get on_board_subTitle {
    return Intl.message(
      'That their affairs cannot be accomplished except by maintaining prayer, and that prayer is a safety and a protection against evil and the gateway to sustenance, and that negligence in it is great in the sight of God, and that he who does not pray will not succeed and none of the affairs of this world or the hereafter will be fulfilled for him.',
      name: 'on_board_subTitle',
      desc: '',
      args: [],
    );
  }

  /// `select client`
  String get select_client {
    return Intl.message(
      'select client',
      name: 'select_client',
      desc: '',
      args: [],
    );
  }

  /// `parent`
  String get parent {
    return Intl.message(
      'parent',
      name: 'parent',
      desc: '',
      args: [],
    );
  }

  /// `child`
  String get child {
    return Intl.message(
      'child',
      name: 'child',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `phone number`
  String get phone {
    return Intl.message(
      'phone number',
      name: 'phone',
      desc: '',
      args: [],
    );
  }

  /// `password`
  String get password {
    return Intl.message(
      'password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `user name`
  String get user_name {
    return Intl.message(
      'user name',
      name: 'user_name',
      desc: '',
      args: [],
    );
  }

  /// `did you forget your password?`
  String get forgetpassword {
    return Intl.message(
      'did you forget your password?',
      name: 'forgetpassword',
      desc: '',
      args: [],
    );
  }

  /// `or`
  String get or {
    return Intl.message(
      'or',
      name: 'or',
      desc: '',
      args: [],
    );
  }

  /// `don't have an account?`
  String get dont_haveanaccount {
    return Intl.message(
      'don\'t have an account?',
      name: 'dont_haveanaccount',
      desc: '',
      args: [],
    );
  }

  /// `create one`
  String get create_account {
    return Intl.message(
      'create one',
      name: 'create_account',
      desc: '',
      args: [],
    );
  }

  /// `please enter required data`
  String get enter_required_data {
    return Intl.message(
      'please enter required data',
      name: 'enter_required_data',
      desc: '',
      args: [],
    );
  }

  /// `Settings`
  String get settings {
    return Intl.message(
      'Settings',
      name: 'settings',
      desc: '',
      args: [],
    );
  }

  /// `Update Profile`
  String get update_profile {
    return Intl.message(
      'Update Profile',
      name: 'update_profile',
      desc: '',
      args: [],
    );
  }

  /// `Change Password`
  String get change_password {
    return Intl.message(
      'Change Password',
      name: 'change_password',
      desc: '',
      args: [],
    );
  }

  /// `Notifications`
  String get notifications {
    return Intl.message(
      'Notifications',
      name: 'notifications',
      desc: '',
      args: [],
    );
  }

  /// `Saved Url`
  String get saved_url {
    return Intl.message(
      'Saved Url',
      name: 'saved_url',
      desc: '',
      args: [],
    );
  }

  /// `Language Settings`
  String get language_settings {
    return Intl.message(
      'Language Settings',
      name: 'language_settings',
      desc: '',
      args: [],
    );
  }

  /// `Determine The Call To Prayer`
  String get azzan {
    return Intl.message(
      'Determine The Call To Prayer',
      name: 'azzan',
      desc: '',
      args: [],
    );
  }

  /// `Information`
  String get info {
    return Intl.message(
      'Information',
      name: 'info',
      desc: '',
      args: [],
    );
  }

  /// `About`
  String get about {
    return Intl.message(
      'About',
      name: 'about',
      desc: '',
      args: [],
    );
  }

  /// `Polices`
  String get polices {
    return Intl.message(
      'Polices',
      name: 'polices',
      desc: '',
      args: [],
    );
  }

  /// `Common Questions`
  String get common_question {
    return Intl.message(
      'Common Questions',
      name: 'common_question',
      desc: '',
      args: [],
    );
  }

  /// `Contact Us`
  String get contact_us {
    return Intl.message(
      'Contact Us',
      name: 'contact_us',
      desc: '',
      args: [],
    );
  }

  /// `Social Media`
  String get social_media {
    return Intl.message(
      'Social Media',
      name: 'social_media',
      desc: '',
      args: [],
    );
  }

  /// `My Account`
  String get profile {
    return Intl.message(
      'My Account',
      name: 'profile',
      desc: '',
      args: [],
    );
  }

  /// `Conversation with the guardian`
  String get talk_to_parent {
    return Intl.message(
      'Conversation with the guardian',
      name: 'talk_to_parent',
      desc: '',
      args: [],
    );
  }

  /// `Sign Out`
  String get logout {
    return Intl.message(
      'Sign Out',
      name: 'logout',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get name {
    return Intl.message(
      'Name',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get emial {
    return Intl.message(
      'Email',
      name: 'emial',
      desc: '',
      args: [],
    );
  }

  /// `Save Changes`
  String get save_changes {
    return Intl.message(
      'Save Changes',
      name: 'save_changes',
      desc: '',
      args: [],
    );
  }

  /// `old password`
  String get old_password {
    return Intl.message(
      'old password',
      name: 'old_password',
      desc: '',
      args: [],
    );
  }

  /// `new password`
  String get new_password {
    return Intl.message(
      'new password',
      name: 'new_password',
      desc: '',
      args: [],
    );
  }

  /// `confirm new password`
  String get confirm_new_password {
    return Intl.message(
      'confirm new password',
      name: 'confirm_new_password',
      desc: '',
      args: [],
    );
  }

  /// `confirm password`
  String get confirm_password {
    return Intl.message(
      'confirm password',
      name: 'confirm_password',
      desc: '',
      args: [],
    );
  }

  /// `Do you have an account?`
  String get do_you_have_an_account {
    return Intl.message(
      'Do you have an account?',
      name: 'do_you_have_an_account',
      desc: '',
      args: [],
    );
  }

  /// `Sign up`
  String get sign_up {
    return Intl.message(
      'Sign up',
      name: 'sign_up',
      desc: '',
      args: [],
    );
  }

  /// `Tell your children and future generations that their affairs cannot be accomplished except by maintaining prayer, and that prayer is a safety and a protection against evil and the gateway to sustenance, and that negligence in it is great in the sight of God, and that he who does not pray will not be wronged and that no matter of the affairs of this world or the hereafter will arise for him.`
  String get about_description {
    return Intl.message(
      'Tell your children and future generations that their affairs cannot be accomplished except by maintaining prayer, and that prayer is a safety and a protection against evil and the gateway to sustenance, and that negligence in it is great in the sight of God, and that he who does not pray will not be wronged and that no matter of the affairs of this world or the hereafter will arise for him.',
      name: 'about_description',
      desc: '',
      args: [],
    );
  }

  /// `confirm`
  String get confirm {
    return Intl.message(
      'confirm',
      name: 'confirm',
      desc: '',
      args: [],
    );
  }

  /// `Enter your name`
  String get enter_name {
    return Intl.message(
      'Enter your name',
      name: 'enter_name',
      desc: '',
      args: [],
    );
  }

  /// `send`
  String get send {
    return Intl.message(
      'send',
      name: 'send',
      desc: '',
      args: [],
    );
  }

  /// `message content`
  String get message_content {
    return Intl.message(
      'message content',
      name: 'message_content',
      desc: '',
      args: [],
    );
  }

  /// `App Language`
  String get app_lang {
    return Intl.message(
      'App Language',
      name: 'app_lang',
      desc: '',
      args: [],
    );
  }

  /// `Please select the application language you want`
  String get app_lang_hint {
    return Intl.message(
      'Please select the application language you want',
      name: 'app_lang_hint',
      desc: '',
      args: [],
    );
  }

  /// `Reset your password`
  String get reset_password {
    return Intl.message(
      'Reset your password',
      name: 'reset_password',
      desc: '',
      args: [],
    );
  }

  /// `Set a new password`
  String get set_new_password {
    return Intl.message(
      'Set a new password',
      name: 'set_new_password',
      desc: '',
      args: [],
    );
  }

  /// `A 4-digit code has been sent to your phone. Please enter the verification code`
  String get set_new_password_hint {
    return Intl.message(
      'A 4-digit code has been sent to your phone. Please enter the verification code',
      name: 'set_new_password_hint',
      desc: '',
      args: [],
    );
  }

  /// `The verification number will be sent to your mobile number`
  String get reset_password_hint {
    return Intl.message(
      'The verification number will be sent to your mobile number',
      name: 'reset_password_hint',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ar'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
