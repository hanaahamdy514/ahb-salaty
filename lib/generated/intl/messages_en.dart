// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "about": MessageLookupByLibrary.simpleMessage("About"),
        "about_description": MessageLookupByLibrary.simpleMessage(
            "Tell your children and future generations that their affairs cannot be accomplished except by maintaining prayer, and that prayer is a safety and a protection against evil and the gateway to sustenance, and that negligence in it is great in the sight of God, and that he who does not pray will not be wronged and that no matter of the affairs of this world or the hereafter will arise for him."),
        "app_lang": MessageLookupByLibrary.simpleMessage("App Language"),
        "app_lang_hint": MessageLookupByLibrary.simpleMessage(
            "Please select the application language you want"),
        "azzan": MessageLookupByLibrary.simpleMessage(
            "Determine The Call To Prayer"),
        "change_password":
            MessageLookupByLibrary.simpleMessage("Change Password"),
        "child": MessageLookupByLibrary.simpleMessage("child"),
        "common_question":
            MessageLookupByLibrary.simpleMessage("Common Questions"),
        "confirm": MessageLookupByLibrary.simpleMessage("confirm"),
        "confirm_new_password":
            MessageLookupByLibrary.simpleMessage("confirm new password"),
        "confirm_password":
            MessageLookupByLibrary.simpleMessage("confirm password"),
        "contact_us": MessageLookupByLibrary.simpleMessage("Contact Us"),
        "create_account": MessageLookupByLibrary.simpleMessage("create one"),
        "do_you_have_an_account":
            MessageLookupByLibrary.simpleMessage("Do you have an account?"),
        "dont_haveanaccount":
            MessageLookupByLibrary.simpleMessage("don\'t have an account?"),
        "emial": MessageLookupByLibrary.simpleMessage("Email"),
        "enter_name": MessageLookupByLibrary.simpleMessage("Enter your name"),
        "enter_required_data":
            MessageLookupByLibrary.simpleMessage("please enter required data"),
        "forgetpassword": MessageLookupByLibrary.simpleMessage(
            "did you forget your password?"),
        "info": MessageLookupByLibrary.simpleMessage("Information"),
        "language_settings":
            MessageLookupByLibrary.simpleMessage("Language Settings"),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "logout": MessageLookupByLibrary.simpleMessage("Sign Out"),
        "message_content":
            MessageLookupByLibrary.simpleMessage("message content"),
        "name": MessageLookupByLibrary.simpleMessage("Name"),
        "new_password": MessageLookupByLibrary.simpleMessage("new password"),
        "notifications": MessageLookupByLibrary.simpleMessage("Notifications"),
        "old_password": MessageLookupByLibrary.simpleMessage("old password"),
        "on_board_subTitle": MessageLookupByLibrary.simpleMessage(
            "That their affairs cannot be accomplished except by maintaining prayer, and that prayer is a safety and a protection against evil and the gateway to sustenance, and that negligence in it is great in the sight of God, and that he who does not pray will not succeed and none of the affairs of this world or the hereafter will be fulfilled for him."),
        "or": MessageLookupByLibrary.simpleMessage("or"),
        "parent": MessageLookupByLibrary.simpleMessage("parent"),
        "password": MessageLookupByLibrary.simpleMessage("password"),
        "phone": MessageLookupByLibrary.simpleMessage("phone number"),
        "polices": MessageLookupByLibrary.simpleMessage("Polices"),
        "profile": MessageLookupByLibrary.simpleMessage("My Account"),
        "reset_password":
            MessageLookupByLibrary.simpleMessage("Reset your password"),
        "reset_password_hint": MessageLookupByLibrary.simpleMessage(
            "The verification number will be sent to your mobile number"),
        "save_changes": MessageLookupByLibrary.simpleMessage("Save Changes"),
        "saved_url": MessageLookupByLibrary.simpleMessage("Saved Url"),
        "select_client": MessageLookupByLibrary.simpleMessage("select client"),
        "send": MessageLookupByLibrary.simpleMessage("send"),
        "set_new_password":
            MessageLookupByLibrary.simpleMessage("Set a new password"),
        "set_new_password_hint": MessageLookupByLibrary.simpleMessage(
            "A 4-digit code has been sent to your phone. Please enter the verification code"),
        "settings": MessageLookupByLibrary.simpleMessage("Settings"),
        "sign_up": MessageLookupByLibrary.simpleMessage("Sign up"),
        "social_media": MessageLookupByLibrary.simpleMessage("Social Media"),
        "talk_to_parent": MessageLookupByLibrary.simpleMessage(
            "Conversation with the guardian"),
        "tell_your_children": MessageLookupByLibrary.simpleMessage(
            "Tell your children and future generations"),
        "update_profile":
            MessageLookupByLibrary.simpleMessage("Update Profile"),
        "user_name": MessageLookupByLibrary.simpleMessage("user name")
      };
}
