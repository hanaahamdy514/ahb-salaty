// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ar locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ar';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "about": MessageLookupByLibrary.simpleMessage("عن التطبيق"),
        "about_description": MessageLookupByLibrary.simpleMessage(
            " \" أخبروا أبنائكم وأجيالكم القادمة أن أمورهم لا تتم إلا بالمحافظة على الصلاة, وأن الصلاة أمان وحصن من المنكرات وبؤابة الرزق وأن التفريط فيها عظيم عند الله وأنه لا يُغلِح من لا يُصلي ولن قم له أمرٍ من أمور الدنيا ولا الآخرة"),
        "app_lang": MessageLookupByLibrary.simpleMessage("لغه التطبيق"),
        "app_lang_hint": MessageLookupByLibrary.simpleMessage(
            "برجاء تحديد لغه التطبيق التي تريدها"),
        "azzan": MessageLookupByLibrary.simpleMessage("تحديد الاذان"),
        "change_password":
            MessageLookupByLibrary.simpleMessage("تغيير كلمه المرور"),
        "child": MessageLookupByLibrary.simpleMessage("الابن"),
        "common_question":
            MessageLookupByLibrary.simpleMessage("الاساله الشائعه"),
        "confirm": MessageLookupByLibrary.simpleMessage("تاكيد"),
        "confirm_new_password":
            MessageLookupByLibrary.simpleMessage("تاكيد كلمه المرور الجديده"),
        "confirm_password":
            MessageLookupByLibrary.simpleMessage("تاكيد كلمه المرور"),
        "contact_us": MessageLookupByLibrary.simpleMessage("تواصل معنا"),
        "create_account":
            MessageLookupByLibrary.simpleMessage("قم بانشاء حساب"),
        "do_you_have_an_account":
            MessageLookupByLibrary.simpleMessage("هل يوجد لديك حساب"),
        "dont_haveanaccount":
            MessageLookupByLibrary.simpleMessage("لا يوجد لديك حساب؟"),
        "emial": MessageLookupByLibrary.simpleMessage("البريد الالكتروني"),
        "enter_name":
            MessageLookupByLibrary.simpleMessage("الرجاء ادخال الاسم"),
        "enter_required_data": MessageLookupByLibrary.simpleMessage(
            "من فضلك ادخل البيانات المطلوبه"),
        "forgetpassword":
            MessageLookupByLibrary.simpleMessage("هل نسيت كلمه المرور؟"),
        "info": MessageLookupByLibrary.simpleMessage("المعللومات"),
        "language_settings":
            MessageLookupByLibrary.simpleMessage("اعدادات اللغه"),
        "login": MessageLookupByLibrary.simpleMessage("تسجيل الدخول"),
        "logout": MessageLookupByLibrary.simpleMessage("تسجيل الخروج "),
        "message_content":
            MessageLookupByLibrary.simpleMessage("محتوي الرساله"),
        "name": MessageLookupByLibrary.simpleMessage("الاسم"),
        "new_password":
            MessageLookupByLibrary.simpleMessage("كلمه المرور الجديده"),
        "notifications": MessageLookupByLibrary.simpleMessage("التنبيهات"),
        "old_password":
            MessageLookupByLibrary.simpleMessage("كلمه المرور القديمه"),
        "on_board_subTitle": MessageLookupByLibrary.simpleMessage(
            "أن أمورهم لا تتم إلا بالمحافظة على الصلاة وأن الصلاة أمان وحصن من المنكرات وبوابة الرزق وأن التفريط فيها عظيم عند االله وأنه لا يُفلِح من لا يُصلي ولن يقم له أمرٍ من أمور الدنيا ولا الآخرة"),
        "or": MessageLookupByLibrary.simpleMessage("او"),
        "parent": MessageLookupByLibrary.simpleMessage("الأب"),
        "password": MessageLookupByLibrary.simpleMessage("كلمه المرور"),
        "phone": MessageLookupByLibrary.simpleMessage("رقم الجوال"),
        "polices":
            MessageLookupByLibrary.simpleMessage("سياسه الاستخدام والشروط"),
        "profile": MessageLookupByLibrary.simpleMessage("حسابي"),
        "reset_password":
            MessageLookupByLibrary.simpleMessage("استعاده كلمه المرور"),
        "reset_password_hint": MessageLookupByLibrary.simpleMessage(
            "سيتم ارسال رقم التحقق علي رقم الجوال"),
        "save_changes": MessageLookupByLibrary.simpleMessage("حفظ التغيرات"),
        "saved_url": MessageLookupByLibrary.simpleMessage("العناوين المحفوظه"),
        "select_client": MessageLookupByLibrary.simpleMessage("اختر المستخدم"),
        "send": MessageLookupByLibrary.simpleMessage("ارسال"),
        "set_new_password":
            MessageLookupByLibrary.simpleMessage("تعيين كلمه مرور جديده"),
        "set_new_password_hint": MessageLookupByLibrary.simpleMessage(
            "لقد تم ارسال رمزا مكونا من 4 ارقام الى هاتفك برجاء ادخال رمز التحقق"),
        "settings": MessageLookupByLibrary.simpleMessage("الاعدادات"),
        "sign_up": MessageLookupByLibrary.simpleMessage("انشاء حساب"),
        "social_media":
            MessageLookupByLibrary.simpleMessage("وسائل التواصل الاجتماعي"),
        "talk_to_parent":
            MessageLookupByLibrary.simpleMessage("محادثه مع ولي الامر "),
        "tell_your_children": MessageLookupByLibrary.simpleMessage(
            " أخبروا أبنائكم وأجيالكم القادمة"),
        "update_profile":
            MessageLookupByLibrary.simpleMessage("تعديل الملف الشخصي"),
        "user_name": MessageLookupByLibrary.simpleMessage("اسم المستخدم")
      };
}
