import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:task1/core/utils/bloc_observer.dart';
import 'package:task1/core/utils/local_db.dart';
import 'package:task1/core/utils/routes.dart';
import 'package:task1/core/utils/themes.dart';
import 'package:task1/features/auth/data/repo/register/register_repo_implementation.dart';
import 'package:task1/features/auth/presentation/view_model/regiter/register_cubit.dart';
import 'package:task1/features/home/presentation/view_model/home_cubit.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/data/repo/change_lang_repo_implementation.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/language_settings/presentation/view_model/language_setting_states.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/data/repo/update_profile_repo_impl.dart';
import 'package:task1/features/home/presentation/views/profile/profile_views/views/update_profile/presentation/view_model/update_profile_cubit.dart';
import 'package:task1/features/welcome/presentation/view_model/select_client/selsect_client_cubit.dart';
import 'core/utils/service_locator.dart';
import 'features/home/presentation/views/profile/profile_views/views/language_settings/presentation/view_model/laanguage_sttings_cubit.dart';
import 'generated/l10n.dart';

void main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);

  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      statusBarIconBrightness: Brightness.dark,
      statusBarColor: Colors.transparent,
    ),
  );
  Bloc.observer = MyBlocObserver();
  setupServiceLocator();
  WidgetsFlutterBinding.ensureInitialized();
  // await EasyLocalization.ensureInitialized();

  await LocalHelper.init();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => HomeCubit()
        ),
        BlocProvider(
            create: (context) =>
                ChangeLanguageCubit(getIt.get<ChangeLanguageRepoImp>())
                  ..getSavedLanguage()),
        BlocProvider(
            create: (context) =>
            UpdateProfileCubit(getIt.get<UpdateProfileRepoImpl>())
             ),
        BlocProvider(
          create: (context) => SelectClientCubit(),
        ),
        BlocProvider(
          create: (context) => RegisterCubit(getIt.get<RegisterRepoImplementation>()),
        ),
      ],
      child: BlocBuilder<ChangeLanguageCubit, ChangeLanguageStates>(
          builder: (context, state) {
        if (state is ChangeLanguageLocalState) {
          return MaterialApp.router(
            supportedLocales: const [Locale("ar"), Locale("en")],
            locale: Locale(state.locale.languageCode),
            localizationsDelegates: const [
              S.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            debugShowCheckedModeBanner: false,
            theme: appTheme(),
            routerConfig: AppRoutes.routes,
          );
        }
        return const SizedBox();
      }),
    );
  }
}
